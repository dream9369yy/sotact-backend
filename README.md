# TAW 
Together Animated WebToon : 라이브 웹툰 제작 툴  

## Introduction

기존 웹툰에 생동감 있는 움직임을 더해 작품 몰입도를 높일 수 있는 **라이브 웹툰 제작 툴**(**TAW**)을 개발한다. 
TAW를 통해 사용자는 여러 애니메이션 효과를 손쉽게 웹툰 이미지에 적용할 수 있으며, 팀원 간 실시간 협업 기능을 지원하여 작업의 생산성을 향상시킬 수 있다.  

### Architecture

---

![architecture](spring/assets/image/sotact-backend-architecture.png)

### Roles

---

![roles](spring/assets/image/mentee-roles.png)

### Details

---  


### [Details about Backend(Spring Boot)](spring/README.md)    
### [Details about Backend(Node JS)](nodejs/README.md)
### [Details about Backend(Python)](python/README.md)
