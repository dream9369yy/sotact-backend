import numpy as np
from glob import glob
import random
import cv2                
import os
from pathlib import Path
from tqdm import tqdm
import torchvision
from torchvision import datasets
import torch
from torch.utils.data.sampler import SubsetRandomSampler
import torchvision.models as models
import torchvision.transforms as transforms
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optimizer
from PIL import ImageFile
from PIL import Image
import IPython
import matplotlib.pyplot as plt
import time
#%matplotlib inline
#%config InlineBackend.figure_format = 'retina'

stmp = 0

def stamp():
    global stmp
    tmp = time.time()
    if stmp != 0:
        print('Time : ', (tmp-stmp))
    stmp = tmp
stamp()

npath = Path("imgclsf").expanduser()
train_path = str(npath.joinpath("train"))
test_path = str(npath.joinpath("test"))

use_cuda = True
IMG_SZ = 224
BATCH_SZ = 10

MEANS = [0.485, 0.456, 0.406]
DEVIS = [0.229, 0.224, 0.225]

train_transform = transforms.Compose([transforms.RandomResizedCrop(IMG_SZ),
                                      transforms.RandomHorizontalFlip(),
                                      transforms.ToTensor(),
                                      transforms.Normalize(MEANS, DEVIS)])

test_transform = transforms.Compose([transforms.Resize(IMG_SZ+1),
                                     transforms.CenterCrop(IMG_SZ),
                                     transforms.ToTensor(),
                                     transforms.Normalize(MEANS, DEVIS)])


training = datasets.ImageFolder(train_path, transform=train_transform)
validation = datasets.ImageFolder(test_path, transform=test_transform)
testing = datasets.ImageFolder(test_path, transform=test_transform)

train_batches = torch.utils.data.DataLoader(training, batch_size=BATCH_SZ, shuffle=True)
valid_batches = torch.utils.data.DataLoader(validation, batch_size=BATCH_SZ)
test_batches = torch.utils.data.DataLoader(testing, batch_size=BATCH_SZ)

model = models.resnet152(pretrained=True)
for param in model.parameters():
    param.requires_grad = False
model.fc = nn.Linear(2048, 4)
print(model)
# GPU 활용
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model.to(device)
if use_cuda:
    model.cuda()
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.005)
ImageFile.LOAD_TRUNCATED_IMAGES = True

def train(n_epochs, train_loader, valid_loader, model, optimizer, criterion, use_cuda, save_path):
    ### 답 작성 부분 ###
    
    # 학습 로스와 검증 로스 목록을 저장할 배열
    train_losses = []
    valid_losses = []
    # 최소 검증 로스를 무한으로 설정
    min_loss = np.Inf
    print("Started Training...")
    # 에폭수만큼 에폭 만복
    for epoch in range(0, n_epochs):
        train_loss = valid_loss = 0
        # 모델을 학습 상태로 변경
        model.train()
        # 로더에서 배치 사이즈로 데이터와 타겟을 불러오며 반복
        for data, target in train_loader:
            # 쿠다 사용가능 시 쿠다에 올리기
            if use_cuda :
                data, target = data.cuda(), target.cuda()
            # 옵티마이저 기울기 초기화
            optimizer.zero_grad()
            # 모델에 학습 시키기
            output = model(data)
            # 로스함 수 계산 및 역전파
            loss = criterion(output, target)
            loss.backward()
            # 옵티마이저에 파라미터 갱신
            optimizer.step()
            # 로스값 누적
            train_loss += loss.item() * data.size(0)
            
        # 모델 평가 상태로 전환
        model.eval()
        # 로더에서 배치 사이즈로 데이터와 타겟을 불러오며 반복
        for data, target in valid_loader:
            # 쿠다 사용가능 시 쿠다에 올리기
            if use_cuda :
                data, target = data.cuda(), target.cuda()
            # 모델에 평가 시키기
            output = model(data)
            # 로스값 계산 후 로스 값 누적하기
            loss = criterion(output, target)
            # 로스값 누적
            valid_loss += loss.item() * data.size(0)
        
        # 학습 로스와 평가 로스를 데이터 수로 나누어 실 평균 로스 계산
        train_loss = train_loss/len(train_loader.sampler)
        valid_loss = valid_loss/len(valid_loader.sampler)
        # 계산된 로스값 각 목록에 추가
        train_losses.append(train_loss)
        valid_losses.append(valid_loss)
        # 학습 결과 출력
        print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f}'.format(
            epoch+1, train_loss, valid_loss))    
        # 검증 로스가 이전 에폭보다 줄었으면
        if valid_loss <= min_loss:
            print('Validation loss decreased ({:.6f} --> {:.6f}).  Saving model ...'.format(
                min_loss, valid_loss))
            # 해당 모델 저장 및 최소 검증 로스 변경
            torch.save(model.state_dict(), save_path)
            min_loss = valid_loss
            
    print("Finished training")
    return model, train_losses, valid_losses

epochs = 20
stamp()
save_transfer = npath.joinpath('model_transfer.pt')
model, train_losses, valid_losses = train(epochs, train_batches, valid_batches, model, optimizer, criterion, use_cuda, save_transfer)
model.load_state_dict(torch.load(save_transfer))
stamp()

def test(test_loader, model, criterion, use_cuda):
    ### 답 작성 부분 ###
    
    # 전체 배열과 정답수를 저장할 종 분류 수 만큼의 리스트 생성
    class_correct = list(0.0 for i in range(4))
    class_total = class_correct.copy()
    test_loss = 0.0
    # 모델 평가 상태로 변환
    model.eval()
    # 
    # 로더에서 배치 사이즈로 데이터와 타겟을 불러오며 반복
    for data, target in test_loader:
        # 쿠다 사용가능 시 쿠다에 올리기
        if use_cuda :
            data, target = data.cuda(), target.cuda()
        # 모델에 평가 시키기
        output = model(data)
        # 로스값 계산 후 로스 값 누적하기
        loss = criterion(output, target)
        # 로스값 누적
        test_loss += loss.item() * data.size(0)
        # 결과에서 최대값의 인덱스 찾기
        _, predict = torch.max(output, 1)
        # prediction과 target의 일치성 체크
        corrects = predict.eq(target.data.view_as(predict))
        # 결과에 대한 차원 축소 실행
        correct = np.squeeze(corrects.cpu().numpy() if use_cuda else corrects.numpy())
        # 배치 사이즈 만큼 돌면서
        for i in range(target.size(0)) :
            # 전체 평가 수 추가 및 정답시 정답 수 추가
            label = target.data[i]
            class_correct[label] += correct[i].item()
            class_total[label] += 1
            
    # 최종 로스 계산
    test_loss = test_loss/len(test_loader.dataset)
    # 결과 출력
    print('Test Loss: {:.6f}\n\n'.format(test_loss))
    print('\nTest Accuracy (Overall): %2d%% (%2d/%2d)' % (
        100. * np.sum(class_correct) / np.sum(class_total),
        np.sum(class_correct), np.sum(class_total)))

stamp()
test(test_batches, model, criterion, use_cuda)
stamp()
