import image
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def receive_png(request):
    png_name = request.FILES['test'].name
    img = image.image_loader(request.FILES['test'])
    res = image.evalImage(img)
    print(int(res))
    return JsonResponse({
        'result': int(res)
    }, json_dumps_params={'ensure_ascii': True})
