# Sotact-Backend for python

## Introduction

이미지 분류와 평가를 담당하는 pytorch와의 pipeline을 담당하는 역할을 합니다.  
client의 image 관련 request를 pytorch 내부의 평가 API를 호출해 JSON 타입으로 response 합니다 

### Architecture

---

![architecture](../spring/assets/image/sotact-backend-architecture.png)

### Version

---
|Project Environment|Version|
|:---:|:---:|
|`python`|3.8.5|   

### Must Be Set in Advance when Deploying Python

---

For Ubuntu 20.04 : Ubuntu 20.04 and other versions of Debian Linux ship with **Python 3 pre-installed.**
```bash
sudo apt update
sudo apt -y upgrade
```

Reference : https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-programming-environment-on-an-ubuntu-20-04-server

but if you want to run other operating system, please pre install python3  
Download Site : https://www.python.org/downloads/

```  

### For Deploy

---

```bash
git clone https://git.swmgit.org/swmaestro/sotact-backend.git
cd sotact-backend
git checkout develop # if master branch is empty
```

For python: run these executions  
(For example : port number is 8000)
```bash
cd python
python3 manage.py runserver 8000
```

### Sotact-BackEnd Contributors

---

구연수 : kbeeys@gmail.com

이윤석 : gktgnjftm@naver.com
