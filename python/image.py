import numpy as np
from glob import glob
import random
import cv2
import os
from pathlib import Path
from tqdm import tqdm
import torchvision
from torchvision import datasets
import torch
from torch.utils.data.sampler import SubsetRandomSampler
import torchvision.models as models
import torchvision.transforms as transforms
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optimizer
from PIL import ImageFile
from PIL import Image
import IPython
import matplotlib.pyplot as plt
import time

stmp = 0


def stamp():
    global stmp
    tmp = time.time()
    if stmp != 0:
        print('Time : ', (tmp-stmp))
    stmp = tmp


stamp()

npath = Path("imgclsf").expanduser()
train_path = str(npath.joinpath("train"))
test_path = str(npath.joinpath("test"))

use_cuda = True
IMG_SZ = 224
BATCH_SZ = 10

MEANS = [0.485, 0.456, 0.406]
DEVIS = [0.229, 0.224, 0.225]

transform = transforms.Compose([transforms.Resize(IMG_SZ+1),
                                transforms.CenterCrop(IMG_SZ),
                                transforms.ToTensor(),
                                transforms.Normalize(MEANS, DEVIS)])


def image_loader(img_path):
    global use_cuda, transform
    image = Image.open(img_path).convert('RGB')
    img = transform(image)[:3, :, :].unsqueeze(0)
    if use_cuda:
        img = img.cuda()
    return img


model = torch.hub.load('pytorch/vision:v0.6.0',
                       'mobilenet_v2', pretrained=True)
for param in model.parameters():
    param.requires_grad = False
model.fc = nn.Linear(1000, 4)
model.load_state_dict(torch.load(npath.joinpath('model_transfer.pt')))
model.eval()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model.to(device)
if use_cuda:
    model.cuda()

ImageFile.LOAD_TRUNCATED_IMAGES = True


def evalImage(img):
    global model
    output = model(img)
    _, predict = torch.max(output, 1)
    return predict[0]

