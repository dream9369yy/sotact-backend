from gensim import models
from http.server import BaseHTTPRequestHandler, HTTPServer
from json import dumps
from urllib.parse import unquote

predic = {
            "0": ["비", "눈", "서리", "내리다", "물줄기"], #
                "1": ["자동차", "차", "자전거", "버스", "택시", "트럭"], #
                    "2": ["천둥", "번쩍", "섬광", "빛", "깜빡"], #
                        "3": ["등장", "나타남", "생성", "추가", "페이드인", "드러남"], #
                            "4": ["퇴장", "사라짐", "삭제", "제거", "페이드아웃", "가려짐"], #
                                "5": ["바퀴", "야구공", "축구공", "농구공", "회전", "토크"], #
                                    "6": ["말풍선", "대화", "효과선", "이펙트"], #
}
model1 = models.fasttext.load_facebook_model('cc.ko.100.bin');
print("model 1 loaded")
model2 = models.fasttext.load_facebook_model('ko.bin')
print("model 2 loaded")
def evaluate(arg):
    maxSim = 0
    maxKey = ''
    for key in predic:
        tmp = []
        for label in predic[key]:
            sim1 = model1.wv.similarity(arg, label)
            sim2 = model2.wv.similarity(arg, label)
            tmp.append((sim1 + sim2) / 2)
        maxVal = max(tmp)
        if maxSim < maxVal:
            maxKey = key
            maxSim = maxVal
    maxKey = "" if maxSim < 0.25 else maxKey
    return maxKey

httpd = None

class RequestHandler(BaseHTTPRequestHandler):
        
    def _send_cors_headers(self):
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
        self.send_header("Access-Control-Allow-Headers", "x-api-key,Content-Type")

    def send_dict_response(self, d):
        self.wfile.write(bytes(dumps(d), "utf8"))

    def do_OPTIONS(self):
        self.send_response(200)
        self._send_cors_headers()
        self.end_headers()

    def do_GET(self):
        self.send_response(200)
        self._send_cors_headers()
        self.end_headers()

        pathList = self.path.split('/')[1]
        if "?" in pathList:
            pathList = unquote(pathList.split('?')[1]);
            if "&" in pathList:
                pathList = pathList.split('&')[0];
            result = str(evaluate(pathList))
            self.wfile.write(dumps({"recom": result}).encode())

    def do_POST(self):
        self.send_response(200)
        self._send_cors_headers()
        self.send_header("Content-Type", "application/json")
        self.end_headers()

        dataLength = int(self.headers["Content-Length"])
        data = self.rfile.read(dataLength)

        print(data)

        response = {"status": "OK"}
        self.send_dict_response(response)


print("Starting server")
httpd = HTTPServer(("0.0.0.0", 8081), RequestHandler)
print("Hosting server on port 8081")
httpd.serve_forever() 
