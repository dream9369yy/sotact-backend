# Sotact-Backend for Spring Boot

## Introduction

프로그램의 통신이나 요청에 대해 비즈니스 로직 처리 및 DB 데이터 관리.  
클라이언트 정보를 통해 프로그램 접근 권한을 관리하고, 클라이언트 간 친구 관계 속에서 라이브 웹툰 동시 제작 기능을 지원을 목표로 하고 있다. 또한 웹사이트 형태로 관리자에게 제공되는 페이지가 존재한다.  
타인은 접근할 수 없는 구조로 설계 프로그램의 관리자가 다뤄야 할 정보를 통계를 통해 조회할 수 있으며, 사용자의 특별한 처리, 그리고 개발이 아닌 관리 차원에서 이루어질 서비스를 종합해 놓은 사이트를 구축하고 있다.

### Architecture

---

![architecture](assets/image/sotact-backend-architecture.png)

### Version

---

|Project Environment|Version|
|:---:|:---:|
|`Spring Boot`|2.3.1|
|`MYSQL`|8.0|
|`maven` |3.6.3|

### Must Be Set in Advance when Deploying Spring Boot Server

---

if you want to build and run successfully sotact-backend Spring Boot Server, must be set MYSQL_URL and MYSQL_USERNAME and MYSQL_ROOT_PASSWORD variables. these variables in spring/src/main/resources/application.yml  

```yaml
spring:
  datasource:
    url: ${MYSQL_URL}
#    url: jdbc:mysql://localhost:3306/jpadb?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
    username: ${MYSQL_USERNAME}
#    username: root
    password: ${MYSQL_ROOT_PASSWORD}
#    password: root
    driver-class-name: com.mysql.cj.jdbc.Driver
...
```

but if you didn't install git and maven before, please install these follow executions  

For Ubuntu: install git and maven   
(It is a Standard for Ubuntu 20.04, package manager : **apt**)

```bash
sudo apt-get install git
sudo apt-get install maven
```

For CentOS : install git and maven    
(It is a Standard for CentOS 8.2, package manager : **yum**)
```bash
sudo yum install git
sudo yum install maven

sudo yum -y install @mysql (mysql 최신버전 기준)
mysql -u root -p (id is root)
ALTER USER 'root'@'localhost' IDENTIFIED BY '원하는 password';

```

For Mac OS : install git and maven 

```bash
brew install git
brew install maven
```

### For Deploy

---

if you want to build and run sotact-backend Spring Boot Server, clone our gitlab repository and move sotact-backend directory and then checkout develop branch 

```bash
git clone https://git.swmgit.org/swmaestro/sotact-backend.git
cd sotact-backend
git checkout develop # if master branch is empty
```

For SpringBoot: run these executions

```bash
cd spring
mvn package -Dmaven.test.skip=true
cd target
java -jar *.jar
```

**default port is 8080**

### Furthermore

---

This is our DataBase Diagram picture.  
if you want to learn more about our sotact-backend server, please check this picture  

![database-diagram](assets/image/sotact-database-diagram.png)  
  
---

if you want to apply monitoring system in sotact-backend project, learn about Spring Boot actuator and Prometheus and Grafana

First, go to pom.xml file and then find spring-boot-starter-actuator and micrometer-registry-prometheus 
and add dependency

Second, go to application.yml file and then add endpoint like below code

```bash
management:
  endpoints:
    web:
      exposure:
        include: health, info, prometheus
```

third, install prometheus and then go to /etc/prometheus/prometheus.yml and edit below code  
For Example, in Ubuntu 20.04  
```bash
global:
  scrape_interval: 15s # 15초마다 metric pulling
  evaluation_interval: 15s 
scrape_configs:
  - job_name: "TAW Application Spring Boot"
    metrics_path: "/actuator/prometheus" # Application prometheus endpoint
    static_configs:
      - targets: ['localhost:8080'] # Application host:port
```
and **prometheus default port is 9090**  
and run prometheus in below code (environment is Ubuntu 20.04)

```bash
sudo prometheus

or

sudo systmectl daemon-reload
sudo systemctl start prometheus
sudo systemctl enable prometheus
sudo systemctl status prometheus
```
Reference :  
1. https://jongmin92.github.io/2019/12/04/Spring/prometheus/  
2. https://meetup.toast.com/posts/237  

fourth, install grafana and then run grafana in below code
```bash
sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl status grafana-server
```
**grafana default port is 3000**, so if you want to change port,
Refer to below 2nd Reference
Reference : 
1. https://grafana.com/docs/grafana/latest/installation/debian/  
2. https://stackoverflow.com/questions/28303978/changing-grafana-port  

### API Documents

---

### [how to use admin-page tutorial](admin-README.md)


### Sotact-BackEnd Contributors

---

구연수 : kbeeys@gmail.com

이윤석 : gktgnjftm@naver.com