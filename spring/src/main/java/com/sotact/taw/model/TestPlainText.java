package com.sotact.taw.model;

import lombok.Data;


@Data
public class TestPlainText {
    private String token;
    private String value;

}
