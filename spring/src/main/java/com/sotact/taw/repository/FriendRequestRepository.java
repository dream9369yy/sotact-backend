package com.sotact.taw.repository;

import com.sotact.taw.domain.FriendRequest;
import com.sotact.taw.domain.FriendRequestPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface FriendRequestRepository extends JpaRepository<FriendRequest, FriendRequestPrimaryKey> {
    List<FriendRequest> findByUserId1(Long userId);

    List<FriendRequest> findByUserId2(Long userId2);
    @Transactional
    void deleteByUserId1AndUserId2(Long userId1, Long userId2);

    @Transactional
    void deleteByUserId1(Long userId1);

    @Transactional
    void deleteByUserId2(Long userId2);
}
