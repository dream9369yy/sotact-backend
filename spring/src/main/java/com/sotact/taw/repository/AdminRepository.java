package com.sotact.taw.repository;

import com.sotact.taw.domain.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, String> {
    Optional<Admin> findByAdminIdAndAdminPassword(String adminId, String adminPassword);
}
