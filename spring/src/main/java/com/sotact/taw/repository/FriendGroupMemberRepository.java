package com.sotact.taw.repository;

import com.sotact.taw.domain.FriendGroupMember;
import com.sotact.taw.domain.FriendGroupMemberPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface FriendGroupMemberRepository extends JpaRepository<FriendGroupMember, FriendGroupMemberPrimaryKey> {
    /*
     * 현재 쓰이지는 않는 FriendGroupMemberRepository interface
     * 주석 처리 안한 이유 : interface 상속으로 해당 메소드들 주석 처리 불가
     * 그로 인해 관련 소스코드가 존재하는 domain 내부 관련한 class 파일, repository 파일 또한 주석 처리 불가
     * */

    @Transactional
    void deleteByUserIdAndAndFriendId(Long userId, Long friendId);

    @Transactional
    void deleteByUserId(Long userId);

    @Transactional
    void deleteByUserIdAndFriendIdAndGroupId(Long userId, Long friendId, Long groupId);

    List<FriendGroupMember> findByUserId(Long userId);
}
