package com.sotact.taw.repository;

import com.sotact.taw.domain.User;
import com.sotact.taw.domain.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByPassword(String password);
    @Query("select new com.sotact.taw.domain.User(u.email,u.nickname,u.myTeam) from User u ")
    List<User> findAll();
    List<User> findByUserId(Long userId);
    Optional<User> findByEmailAndEmailCheckNumber(String email, String emailCheckNumber);
    long countByEmailAndUserType(String email, UserType userType);
    long countByNickname(String nickname);
    Optional<User> findByEmailAndUserType(String email, UserType userType);
    Optional<User> findByOauthId(String userId);
    List<User> findByEmailContainingOrNicknameContaining(String email, String nickname);
}
