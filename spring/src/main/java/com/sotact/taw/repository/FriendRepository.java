package com.sotact.taw.repository;

import com.sotact.taw.domain.Friend;
import com.sotact.taw.domain.FriendPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface FriendRepository extends JpaRepository<Friend, FriendPrimaryKey> {
    Optional<Friend> findByUserIdAndFriendId(Long userId, Long friendId);

    List<Friend> findByUserId(Long userId);

    @Transactional
    void deleteByUserIdAndAndFriendId(Long userId,Long friendId);

    @Transactional
    void deleteByUserId(Long userId);

    @Transactional
    void deleteByFriendId(Long friendId);

    long countByUserIdAndFriendId(Long userId,Long friendId);


}
