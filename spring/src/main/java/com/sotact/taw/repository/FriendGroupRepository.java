package com.sotact.taw.repository;

import com.sotact.taw.domain.FriendGroup;
import com.sotact.taw.domain.FriendGroupPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface FriendGroupRepository extends JpaRepository<FriendGroup, FriendGroupPrimaryKey> {
    /*
     * 현재 쓰이지는 않는 FriendGroupRepository interface
     * 주석 처리 안한 이유 : interface 상속으로 해당 메소드들 주석 처리 불가
     * 그로 인해 관련 소스코드가 존재하는 domain 내부 관련한 class 파일, repository 파일 또한 주석 처리 불가
     * */

    FriendGroup findByGroupId(Long groupId);

    @Transactional
    void deleteByUserId(Long userId);

    @Transactional
    void deleteByUserIdAndGroupId(Long userId, Long groupId);

    List<FriendGroup> findByUserId(Long userId);
}
