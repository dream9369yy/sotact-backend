package com.sotact.taw.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sotact.taw.util.ResponseMessage;
import com.sotact.taw.util.StatusCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DefaultRes<T> {
    private int status;
    private String message;
    private T data;

    public DefaultRes(final int status, final String message) {
        this.status = status;
        this.message = message;
        this.data = null;
    }

    public static <T> DefaultRes<T> res(final int status, final String message) {
        return res(status, message, null);
    }

    public static <T> DefaultRes<T> res(final int status, final String message, final T t) {
        return DefaultRes.<T>builder()
                .data(t)
                .status(status)
                .message(message)
                .build();
    }

    public static final DefaultRes FAIL_DEFAULT_RES = new DefaultRes(StatusCode.INTERNAL_SERVER_ERROR, ResponseMessage.INTERNAL_SERVER_ERROR);
    public static final DefaultRes FAIL_POST = new DefaultRes(StatusCode.POST_ERROR, ResponseMessage.POST_ERROR);
    public static final DefaultRes FAIL_LIKE = new DefaultRes(StatusCode.LIKE_ERROR, ResponseMessage.LIKE_ERROR);
    public static final DefaultRes FAIL_DELETE = new DefaultRes(StatusCode.DELETE_ERROR, ResponseMessage.DELETE_ERROR);

    public static final DefaultRes OK = new DefaultRes(StatusCode.OK, ResponseMessage.OK);
    public static final DefaultRes SUCCESS_LOGIN = new DefaultRes(StatusCode.OK, ResponseMessage.LOGIN_SUCCESS);
    public static final DefaultRes SUCCESS_SIGNUP = new DefaultRes(StatusCode.OK, ResponseMessage.CREATED_USER);
    public static final DefaultRes DUPLICATE_EMAIL = new DefaultRes(StatusCode.OK, ResponseMessage.DUPLICATE_EMAIL);
    public static final DefaultRes DUPLICATE_NICKNAME = new DefaultRes(StatusCode.OK, ResponseMessage.DUPLICATE_NICKNAME);
    public static final DefaultRes SUCCESS_FOUND_USER = new DefaultRes(StatusCode.OK, ResponseMessage.SUCCESS_FOUND_USER);

    public static final DefaultRes FAIL_DEFAULT_REQ = new DefaultRes(StatusCode.BAD_REQUEST, ResponseMessage.REQUEST_ERROR);
    public static final DefaultRes FAIL_LOGIN = new DefaultRes(StatusCode.OK, ResponseMessage.LOGIN_FAIL);
    public static final DefaultRes FAIL_FOUND_USER = new DefaultRes(StatusCode.OK, ResponseMessage.NOT_FOUND_USER);
    public static final DefaultRes UNCHECKED_EMAIL = new DefaultRes(StatusCode.OK, ResponseMessage.UNCHECKED_EMAIL);
    public static final DefaultRes FAIL_SEND_EMAIL = new DefaultRes(StatusCode.INTERNAL_SERVER_ERROR, ResponseMessage.EMAIL_ERROR);
    public static final DefaultRes FAIL_CREATE_USER = new DefaultRes(StatusCode.INTERNAL_SERVER_ERROR, ResponseMessage.FAIL_CREATE_USER);
    public static final DefaultRes FAIL_DELETE_USER = new DefaultRes(StatusCode.INTERNAL_SERVER_ERROR, ResponseMessage.FAIL_DELETE_USER);
    public static final DefaultRes SUCCESS_USERDEL = new DefaultRes(StatusCode.OK, ResponseMessage.USERDEL_SUCCESS);

}
