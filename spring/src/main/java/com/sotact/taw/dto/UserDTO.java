package com.sotact.taw.dto;

import com.sotact.taw.domain.User;
import com.sotact.taw.domain.UserType;
import com.sotact.taw.domain.emailCheck;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Data
public class UserDTO {
    private long userId;

    @NotBlank(message = "email can not be blanked.                     ")
    @Pattern(regexp="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$",
            message = "incorrect form of email")
    private String email;

    @NotBlank(message = "password can not be blanked.                  ")
    @Pattern(regexp="(?=.*[0-9])(?=.*[a-zA-Z])(?=.*\\W)(?=\\S+$).{8,20}",
//            message = "비밀번호는 영문 대,소문자와 숫자, 특수기호가 적어도 1개 이상씩 포함된 8자 ~ 20자로 입력해주세요")
            message = "please write between 8~20 letters(including one English, one number and one special character).                  ")
    private String password;

    @NotBlank(message = "nickname can not be blanked.                 ")
    private String nickname;

    private String profilePath;
    private emailCheck emailCheck;
    private String myTeam;
    private String emailCheckNumber;
    private UserType userType;
    private String oauthId;

    // DTO field
    @NotBlank(message = "please repeat your password.                  ")
    private String passwordConfirm;

    public User toEntity(){
        return User.builder()
                .password(password)
                .email(email)
                .nickname(nickname)
                .emailCheck(emailCheck)
                .emailCheckNumber(emailCheckNumber)
                .userType(userType)
                .build();
    }
    public User toOauthEntity(){
        User user = this.toEntity();
        user.setOauthId(this.getOauthId());
        user.setProfilePath(this.getProfilePath());
        return user;
    }
}
