package com.sotact.taw.serviceImpl;


import com.sotact.taw.domain.Friend;
import com.sotact.taw.domain.FriendGroup;
import com.sotact.taw.domain.FriendGroupMember;
import com.sotact.taw.domain.User;
import com.sotact.taw.dto.DefaultRes;
import com.sotact.taw.repository.FriendGroupMemberRepository;
import com.sotact.taw.repository.FriendGroupRepository;
import com.sotact.taw.repository.FriendRepository;
import com.sotact.taw.repository.UserRepository;
import com.sotact.taw.service.FriendService;
import com.sotact.taw.util.ResponseMessage;
import com.sotact.taw.util.StatusCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
@Service
public class FriendServiceImpl implements FriendService {


    @Autowired
    private FriendRepository friendRepository;

    @Autowired
    private FriendRequestServiceImpl friendRequestService;

    @Autowired
    private UserRepository userRepository;

/*    @Autowired
    private FriendGroupRepository friendGroupRepository;

    @Autowired
    private FriendGroupMemberRepository friendGroupMemberRepository;*/
    /*
    * FriendGroup, FriendGroupMember 관련 데이터까지 한꺼번에 수행하려 했으나
    * 현재는 FriendGroup, FriendGroupMember 관련 코드 수행이 없으므로 관련 부분 주석처리
    * */
    public DefaultRes<?> findList(long userId) throws JSONException {

        List<Friend> friendList;
/*        List<FriendGroup> friendGroupList;
        List<FriendGroupMember> friendGroupMemberList;*/
        try {
            friendList = friendRepository.findByUserId(userId);
/*            friendGroupList = friendGroupRepository.findByUserId(userId);
            friendGroupMemberList = friendGroupMemberRepository.findByUserId(userId);*/
        } catch (NoSuchElementException e) {
            log.error("friend findList error: " + e.getMessage());
            return DefaultRes.res(StatusCode.DB_ERROR, ResponseMessage.DB_ERROR);
        }
        JSONArray jsonArray = new JSONArray();
        JSONArray tempJsonArray = new JSONArray();
        for (Friend friend : friendList) {
            JSONObject jsonObject = new JSONObject();
            long friendId = friend.getFriendId();
            String nickname = friend.getUser2().getNickname();
            jsonObject.put("friendId", friendId);
            jsonObject.put("nickname", nickname);
            tempJsonArray.put(jsonObject);
        }
        jsonArray.put(tempJsonArray);
        /*
        * friend 관련 :
        * friendGroup 및 friendgroupMember 관련한 로직 삭제
        * */
/*        tempJsonArray = new JSONArray();
        for (FriendGroup friendGroup : friendGroupList) {
            JSONObject jsonObject = new JSONObject();
            long groupId = friendGroup.getGroupId();
            String groupName = friendGroup.getGroupName();
            jsonObject.put("groupId", groupId);
            jsonObject.put("groupName", groupName);
            tempJsonArray.put(jsonObject);
        }
        jsonArray.put(tempJsonArray);
        tempJsonArray = new JSONArray();
        for (FriendGroupMember friendGroupMember : friendGroupMemberList) {
            JSONObject jsonObject = new JSONObject();
            long groupId = friendGroupMember.getGroupId();
            long friendId = friendGroupMember.getFriendId();
            jsonObject.put("groupId", groupId);
            jsonObject.put("friendId", friendId);
            tempJsonArray.put(jsonObject);
        }
        jsonArray.put(tempJsonArray);
        */
        return DefaultRes.res(StatusCode.OK, ResponseMessage.LIST_FRIEND, jsonArray.toString());
    }

    @Override
    public DefaultRes<?> deleteFriend(long userId, String userIdList) {
        String[] userList = userIdList.split(",");
        for (String value : userList) {
            long friendId = Integer.parseInt(value);
            try {
                friendRepository.deleteByUserIdAndAndFriendId(userId, friendId);
            } catch (Exception e) {
                log.error("friendRequest delete error : " + e.getMessage());
                return DefaultRes.res(StatusCode.DELETE_ERROR, ResponseMessage.DELETE_ERROR);
            }
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.DELETE_FRIEND);
    }

    @Transactional
    public DefaultRes<?> postFriendRequest(String acceptStatus, long userId1, String postUserIdList) {
        String[] userList = postUserIdList.split(",");
        /*수락이 거절이면 그냥 삭제를 하면된다*/
        if (acceptStatus.equals("NO")) {
            friendRequestService.deleteFriend(userId1, postUserIdList);
            return DefaultRes.res(StatusCode.OK, ResponseMessage.DENY_FRIEND); //여기는 수정이 필요
        }
        else {
            for (String value : userList) {
                long userId2 = Long.parseLong(value);
                Optional<User> user = userRepository.findById(userId1);
                Optional<User> user2 = userRepository.findById(userId2);
                if (friendRepository.countByUserIdAndFriendId(userId1, userId2) > 0) {
                    log.info("이미 등록된 친구입니다. 건너뜁니다");
                    continue;
                }
                if (user.isPresent() && user2.isPresent()) {
                    User user1 = user.get();
                    User user3 = user2.get();
                    Friend friend = new Friend(userId1, userId2, user1, user3);
                    Friend friend1 = new Friend(userId2, userId1, user3, user1);
                    friend.setUser(user1);
                    friend1.setUser(user3);
                    try {
                        friendRepository.save(friend);
                        friendRepository.save(friend1);
                    } catch (Exception e) {
                        log.error("friend post error : " + e.getMessage());
                        return DefaultRes.res(StatusCode.POST_ERROR, ResponseMessage.POST_ERROR);
                    }
                }
            }
            /*등록이 다 되면, 친구 요청에 관한것을 한꺼번에 삭제를 해준다*/
            friendRequestService.deleteFriend(userId1, postUserIdList);
            return DefaultRes.res(StatusCode.OK, ResponseMessage.CREATED_FRIEND);
        }
    }
}
