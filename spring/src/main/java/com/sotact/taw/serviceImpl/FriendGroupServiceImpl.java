package com.sotact.taw.serviceImpl;

import com.sotact.taw.domain.FriendGroup;
import com.sotact.taw.domain.User;
import com.sotact.taw.dto.DefaultRes;
import com.sotact.taw.repository.FriendGroupRepository;
import com.sotact.taw.repository.UserRepository;
import com.sotact.taw.service.FriendService;
import com.sotact.taw.util.ResponseMessage;
import com.sotact.taw.util.StatusCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class FriendGroupServiceImpl implements FriendService {
    /*
    * 현재 쓰이지는 않는 FriendGroupServiceImpl class
    * 주석 처리 안한 이유 : interface 상속으로 해당 메소드들 주석 처리 불가
    * 그로 인해 관련 소스코드가 존재하는 domain 내부 관련한 class 파일, repository 파일 또한 주석 처리 불가
    * */
    @Autowired
    private FriendGroupRepository friendGroupRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public DefaultRes<?> findList(long userId) throws JSONException {
        List<FriendGroup> friendList;
        try {
            friendList = friendGroupRepository.findByUserId(userId);
        } catch (NoSuchElementException e) {
            log.error("friendGroup findList error: " + e.getMessage());
            return DefaultRes.res(StatusCode.DB_ERROR, ResponseMessage.DB_ERROR);
        }
        JSONArray jsonArray = new JSONArray();
        for (FriendGroup friendgroup : friendList) {
            JSONObject jsonObject = new JSONObject();
            long groupId = friendgroup.getGroupId();
            String groupName = friendgroup.getGroupName();
            jsonObject.put("groupId", groupId);
            jsonObject.put("groupName", groupName);
            jsonArray.put(jsonObject);
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.READ_USER, jsonArray.toString());
    }

    @Transactional
    public DefaultRes<?> postFriendGroup(long userId, String postGroupNameList) {
        String[] groupNameList = postGroupNameList.split(",");
        for (String groupName : groupNameList) {
            Optional<User> user = userRepository.findById(userId);
            if (user.isPresent()) {
                FriendGroup friendGroup = new FriendGroup();
                friendGroup.setGroupName(groupName);
                friendGroup.setUserId(userId);
                friendGroup.setUser5(user.get());
                try {
                    friendGroupRepository.save(friendGroup);
                } catch (Exception e) {
                    log.error("friendGroup post error " + e.getMessage());
                    return DefaultRes.res(StatusCode.POST_ERROR, ResponseMessage.POST_ERROR);
                }
            }
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.CREATED_FRIEND);
    }

    @Transactional
    public DefaultRes<?> deleteFriend(long userId, String userIdList) {
        String[] userList = userIdList.split(",");
        for (String value : userList) {
            long groupId = Long.parseLong(value);
            try {
                friendGroupRepository.deleteByUserIdAndGroupId(userId, groupId);
            } catch (Exception e) {
                log.error("friendGroup delete error : " + e.getMessage());
                return DefaultRes.res(StatusCode.DELETE_ERROR, ResponseMessage.DELETE_ERROR);
            }
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.DELETE_USER);
    }
}
