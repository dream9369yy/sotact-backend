package com.sotact.taw.serviceImpl;

import com.fasterxml.jackson.databind.JsonNode;
import com.sotact.taw.domain.User;
import com.sotact.taw.domain.UserType;
import com.sotact.taw.domain.emailCheck;
import com.sotact.taw.dto.UserDTO;
import com.sotact.taw.exception.EmailSendException;
import com.sotact.taw.exception.IncompleteEmailCheck;
import com.sotact.taw.exception.OAuthException;
import com.sotact.taw.exception.PasswordMismatchException;
import com.sotact.taw.manager.OAuthManager;
import com.sotact.taw.repository.UserRepository;
import com.sotact.taw.service.OAuthService;
import com.sotact.taw.service.UserService;
import com.sotact.taw.util.UUIDUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JavaMailSender mailSender;

    private final OAuthManager oauthManager;

    /**
     * 회원가입을 신청한 유저 정보를 저장
     *
     * @param user 회원가입 입력 정보
     * @return 회원정보 DB 입력 성공여부
     */
    @Override
//    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Transactional
    public boolean registerAccount(UserDTO user) throws EmailSendException {

        String encryptPwd = passwordEncoder.encode(user.getPassword());     // 비밀번호 암호화
        user.setPassword(encryptPwd);

        String emailRandKey = UUIDUtil.createUUID(32);
        user.setEmailCheckNumber(emailRandKey);
        user.setEmailCheck(emailCheck.NOT);
        user.setUserType(UserType.TAW);

        User save = save = userRepository.save(user.toEntity());
        sendEmailKey(save.getEmail(), emailRandKey);

        return true;
    }

    /**
     * 회원 가입시 입력한 이메일로 인증 메일 송신
     *
     * @param email        메일을 보낼 계정
     * @param emailRandKey 인증키 난수
     */
    @Override
    public void sendEmailKey(String email, String emailRandKey) throws EmailSendException {

        MimeMessage mail = mailSender.createMimeMessage();
        // FIXME : URL DOMAIN 추후 변경 요망
        String mailText = "<p>Welcome ! </p><br>"
                + "<p>Thank you for creating your TAW Account</p><br>"
                + "<p>To complete your registration, click the link below</p><br>"
                + "<p><a href='http://taw.esllo.com:8080/user/complete-signup?email=" + email + "&key=" + emailRandKey + "'>click link</a></p><br>"
                + "<p>TAW Team</p><br>";
        try {

            mail.setSubject("Complete your TAW account registration", "utf-8");
            mail.setText(mailText, "utf-8", "html");
            mail.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            mailSender.send(mail);

        } catch (MessagingException e) {
            throw new EmailSendException();
        }

    }

    /**
     * 이메일 인증을 통한 계정 활성화
     *
     * @param email 인증할 이메일 계정
     * @param key   인증키
     * @return
     */
    @Override
    public User signUpComplete(String email, String key) {
        Optional<User> user = userRepository.findByEmailAndEmailCheckNumber(email, key);
        if (user.isPresent()) {
            // 이메일 인증 확인
            user.get().setEmailCheck(emailCheck.OK);
            userRepository.save(user.get());
        } else {
            user.orElseThrow(IllegalArgumentException::new);
        }
        return user.get();
    }

    /**
     * 자체 로그인 계정 이메일 중복 확인
     *
     * @param email 체크할 이메일
     * @return 이메일 사용 가능 여부
     * {
     * True : 사용 가능,
     * False : 사용 불가능(중복)
     * }
     */
    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public boolean duplicateEmail(String email) {
        long count = userRepository.countByEmailAndUserType(email, UserType.TAW);
        if (count > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 사용자 닉네임 중복 확인
     * 
     * @param nickname 체크할 닉네임
     * @return 닉네임 사용가능 여부
     */
    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public boolean duplicateNickname(String nickname) {
        long count = userRepository.countByNickname(nickname);
        if(count > 0) return false;
        return true;
    }

    /**
     * 엑세스 토큰(인증 수단)을 가져와 사용자 정보 확인
     *
     * @param provider OAuth 종류
     * @param code     인증 코드
     * @return
     */
    @Deprecated
    @Override
    public User getOAuthUserInfo(String provider, String code) throws Exception, OAuthException {
        String userId = "";
        OAuthService oAuthService = null;
        String accessToken = "";
        JsonNode oAuthUserInfo = null;
        try {
            oAuthService = oauthManager.getServiceObject(provider);
            accessToken = oAuthService.getAccessToken(code);

            oAuthUserInfo = oAuthService.getOAuthUserInfo(accessToken);
            userId = oAuthUserInfo.get("id").asText();
            try {
                System.out.println("id -> " + userId);
                System.out.println("bring info -> " + oAuthUserInfo.toString());
            }catch (Exception e){
                e.printStackTrace();
                System.out.println("오어스에서 정보를 못가져옴");
            }
        } catch (Exception e) {
            throw new OAuthException(e.getMessage());
        }
        Optional<User> user = userRepository.findByOauthId(userId);
//        try {
//            System.out.println("get() 메서드 가져오는 정보 확인 : " + user.get().toString());
//        } catch (Exception e){
//            System.out.println("get() 메서드 가져오는 정보 확인 에러 Optional 확인\n\n\n\n\n\n\n"+e.getMessage()+"\n\n\n\n\n\n\n\n\n\n");
//
//        }
        if (user.isPresent()) {
            System.out.println();
            return user.get();
        } else {
            User test = null;
            try {
                test = userRepository.save(oAuthService.saveOAuthUserInfo(oAuthUserInfo));       // 각 OAuth에 정보에 맞춰 객체 생성 후 정보 DB에 저장
            } catch (Exception e){
                System.out.println("user 정보 디비에 저장 실패");
            }
            return test;
        }
    }

    /**
     * JavaScript 방식 OAuth 로그인
     */
    @Override
    public User oauthLogin(String provider, UserDTO user) throws Exception {
        OAuthService oAuthService = oauthManager.getServiceObject(provider);

        String oauthId = user.getOauthId();
        Optional<User> oauthUser = userRepository.findByOauthId(oauthId);

        if (oauthUser.isPresent()) {
            return oauthUser.get();
        } else {
            // 객체 세팅
            User signUp = oAuthService.saveOAuthUserInfo(user);
            return userRepository.save(signUp);       // 각 OAuth에 맞춰 객체 생성 후 정보 DB에 저장
        }
    }

    @Override
    public User checkAccount(String email, String password, UserType userType) throws PasswordMismatchException, IncompleteEmailCheck {

        User user = userRepository.findByEmailAndUserType(email, userType).get();

        // 이메일 인증 미완료
        if (user.getEmailCheck() != emailCheck.OK) {
            throw new IncompleteEmailCheck();
        }

        // 비밀번호 불일치
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new PasswordMismatchException();
        }
        return user;
        /*
            FIXME : 주어야할 정보 제한
            SUCCESS_LOGIN: userId, email, nickname, profilePath, myTeam, usreType
        */
    }

    @Override
    public void deleteUser(Long userId) {
        User user = userRepository.findById(userId).get();
        userRepository.delete(user);
    }

    @Override
    public List<User> findByEmailORNickname(String keyword) {
        List<User> search = userRepository.findByEmailContainingOrNicknameContaining(keyword, keyword);
        return search;
    }
}
