package com.sotact.taw.serviceImpl;

import com.sotact.taw.domain.Friend;
import com.sotact.taw.domain.FriendGroup;
import com.sotact.taw.domain.FriendGroupMember;
import com.sotact.taw.domain.User;
import com.sotact.taw.dto.DefaultRes;
import com.sotact.taw.repository.FriendGroupMemberRepository;
import com.sotact.taw.repository.FriendGroupRepository;
import com.sotact.taw.repository.FriendRepository;
import com.sotact.taw.repository.UserRepository;
import com.sotact.taw.util.ResponseMessage;
import com.sotact.taw.util.StatusCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class FriendGroupMemberServiceImpl {
    /*
     * 현재 쓰이지는 않는 FriendGroupMemberServiceImpl class
     * 주석 처리 안한 이유 : interface 상속으로 해당 메소드들 주석 처리 불가
     * 그로 인해 관련 소스코드가 존재하는 domain 내부 관련한 class 파일, repository 파일 또한 주석 처리 불가
     * */

    @Autowired
    private FriendGroupMemberRepository friendGroupMemberRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FriendRepository friendRepository;

    @Autowired
    private FriendGroupRepository friendGroupRepository;

    @Transactional
    public DefaultRes<?> postFriendGroupMember(long userId, long groupId, String friendGroupMemberList) {
        String[] groupNameList = friendGroupMemberList.split(",");
        for (String groupMember : groupNameList) {
            long groupMemberId = Long.parseLong(groupMember);
            Optional<User> user = userRepository.findById(userId);//그룹에 추가하는 유저
            Optional<User> user2 = userRepository.findById(groupMemberId); // 그룹에 추가당하는 유저
            Optional<FriendGroup> friendGroup = Optional.ofNullable(friendGroupRepository.findByGroupId(groupId));
            Optional<Friend> friend = friendRepository.findByUserIdAndFriendId(userId, groupMemberId);
            if (user.isPresent() && user2.isPresent() && friendGroup.isPresent() && friend.isPresent()) {
                FriendGroupMember friendGroupMember = new FriendGroupMember(userId, groupMemberId, groupId, friendGroup.get(), friend.get());
                try {
                    friendGroupMemberRepository.save(friendGroupMember);
                } catch (Exception e) {
                    log.error("friendGroupMember post error " + e.getMessage());
                    return DefaultRes.res(StatusCode.POST_ERROR, ResponseMessage.POST_ERROR);
                }
            }
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.CREATED_FRIEND);
    }

    @Transactional
    public DefaultRes<?> deleteFriendGroupMember(long userId, long groupId, String friendGroupMemberList) {
        String[] userList = friendGroupMemberList.split(",");
        for (String value : userList) {
            long groupFriendId = Long.parseLong(value);
            try {
                friendGroupMemberRepository.deleteByUserIdAndFriendIdAndGroupId(userId, groupFriendId, groupId);
            } catch (Exception e) {
                log.error("friendGroupMember delete error : " + e.getMessage());
                return DefaultRes.res(StatusCode.DELETE_ERROR, ResponseMessage.DELETE_ERROR);
            }
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.DELETE_USER);
    }
}
