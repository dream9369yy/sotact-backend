package com.sotact.taw.serviceImpl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sotact.taw.domain.User;
import com.sotact.taw.domain.UserType;
import com.sotact.taw.domain.emailCheck;
import com.sotact.taw.dto.UserDTO;
import com.sotact.taw.properties.OAuthProperties;
import com.sotact.taw.service.OAuthService;
import lombok.RequiredArgsConstructor;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class KaKaoServiceImpl implements OAuthService {

    private final OAuthProperties properties;

    public static final String provider = "kakao";

    /**
     * Kakao OAuth 로그인 페이지 Url 생성
     *
     * @return Kakao Oauth Login URL
     */
    @Override
    public String getRedirectUrl() {

        String clientId = properties.getKakao().get("client-id");
//        String redirectURI = "http://localhost:8080/user/kakao-login";
        String redirectURI = "http://taw.esllo.com:8080/user/kakao-login";

        StringBuffer url = new StringBuffer();
        url.append("https://kauth.kakao.com/oauth/authorize?");
        url.append("client_id=" + clientId);
        url.append("&redirect_uri=" + redirectURI);
        url.append("&response_type=code");
        url.append("&scope=account_email profile");     // add

        return url.toString();
    }

    /**
     * 사용자 토큰 정보 받기
     *
     * @param code 인증 코드 받기 요청으로 얻은 인증 코드
     * @return 사용자 인증 토큰
     */
    @Override
    public String getAccessToken(String code) {

        String clientId = properties.getKakao().get("client-id");

        String redirectURI = properties.getKakao().get("redirect-uri");
        String requestURI = "https://kauth.kakao.com/oauth/token";

        final List<NameValuePair> params = new ArrayList<>();

        params.add(new BasicNameValuePair("grant_type", "authorization_code"));
        params.add(new BasicNameValuePair("client_id", clientId)); // REST API KEY
        params.add(new BasicNameValuePair("redirect_uri", redirectURI));
        params.add(new BasicNameValuePair("code", code));

        JsonNode returnNode = null;

        try {
            HttpPost request = new HttpPost(requestURI);
            request.setEntity(new UrlEncodedFormEntity(params));
            request.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            int responseCode = response.getStatusLine().getStatusCode();

            System.out.println("Sending 'POST' request to URL : " + requestURI);
            System.out.println("Post parameters : " + params);
            System.out.println("Response Code : " + responseCode);

            // JSON 형태 반환값 처리
            ObjectMapper mapper = new ObjectMapper();
            returnNode = new ObjectMapper().readTree(response.getEntity().getContent());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnNode.get("access_token").textValue();
    }

    /**
     * temp : 고유 아이디 값과 토큰의 상세 정보
     */
    public JsonNode getOAuthUserInfo2(String accessToken) {

        String requestURI = "https://kapi.kakao.com/v1/user/access_token_info";

        JsonNode returnNode = null;

        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(requestURI);
            request.addHeader("Authorization", "Bearer " + URLEncoder.encode(accessToken, "UTF-8"));

            HttpResponse response = client.execute(request);

            int responseCode = response.getStatusLine().getStatusCode();
            String msg = response.getStatusLine().getReasonPhrase();

            System.out.println("Sending 'GET' request to URL : " + requestURI);
            System.out.println("Response Code : " + responseCode);
            System.out.println("Response Message : " + msg);

            ObjectMapper mapper = new ObjectMapper();
            returnNode = mapper.readTree(response.getEntity().getContent());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnNode;
    }

    /**
     * 사용자 정보 확인
     *
     * @param accessToken 사용자 인증 토큰
     * @return 사용자 정보
     */
    @Override
    public JsonNode getOAuthUserInfo(String accessToken) {

        String requestURI = "https://kapi.kakao.com/v2/user/me";

        JsonNode returnNode = null;

        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(requestURI);

            post.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
            post.setHeader("Authorization", "Bearer " + URLEncoder.encode(accessToken, "UTF-8"));

            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            String msg = response.getStatusLine().getReasonPhrase();

            System.out.println("\nSending 'POST' request to URL : " + requestURI);
            System.out.println("Response Code : " + responseCode);

            ObjectMapper mapper = new ObjectMapper();
            returnNode = mapper.readTree(response.getEntity().getContent());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnNode;
    }

    /**
     * 카카오 계정 정보 추출
     *
     * @param user
     * @return
     */
    @Override
    public User saveOAuthUserInfo(JsonNode user) {

        String email = user.get("kakao_account").get("email").asText();
        String password = "temp";           // FIXME : password Null 허용?
        String nickname = user.get("properties").get("nickname").asText()+"_kakao";
        String oauthId = user.get("id").asText();
        String profilePath = user.get("properties").get("profile_image").asText();
        UserType userType = UserType.KAKAO;

        User userInfo = new User(password, email, nickname, emailCheck.OK, "oauth", userType);
        userInfo.setOauthId(oauthId);
        userInfo.setProfilePath(profilePath);
        System.out.println("new kakao oauth account : "+userInfo.toString());
        return userInfo;
    }

    @Override
    public User saveOAuthUserInfo(UserDTO user) {
        user.setPassword("temp");
        user.setEmailCheck(emailCheck.OK);
        user.setEmailCheckNumber("oauth");
        user.setUserType(UserType.KAKAO);

        return user.toOauthEntity();
    }
}
