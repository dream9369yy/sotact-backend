package com.sotact.taw.serviceImpl;

import com.sotact.taw.domain.FriendRequest;
import com.sotact.taw.domain.User;
import com.sotact.taw.domain.acceptStatus;
import com.sotact.taw.dto.DefaultRes;
import com.sotact.taw.repository.FriendRequestRepository;
import com.sotact.taw.repository.UserRepository;
import com.sotact.taw.service.FriendService;
import com.sotact.taw.util.ResponseMessage;
import com.sotact.taw.util.StatusCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class FriendRequestServiceImpl implements FriendService {
    @Autowired
    private FriendRequestRepository friendRequestRepository;
    @Autowired
    private UserRepository userRepository;

    public DefaultRes<?> findList(long userId) throws JSONException {
        List<FriendRequest> friendRequestList;
        try {
            friendRequestList = friendRequestRepository.findByUserId2(userId);
        } catch (NoSuchElementException e) {
            log.error("friendRequest findList error: " + e.getMessage());
            return DefaultRes.res(StatusCode.DB_ERROR, ResponseMessage.DB_ERROR);
        }
        JSONArray jsonArray = new JSONArray();
        for (FriendRequest friend : friendRequestList) {
            JSONObject jsonObject = new JSONObject();
            long friendId = friend.getUserId1();
            String nickname = friend.getUser3().getNickname();
            jsonObject.put("friendId", friendId);
            jsonObject.put("nickname", nickname);
            jsonArray.put(jsonObject);
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.READ_USER, jsonArray.toString());
    }

    @Override
    @Transactional
    public DefaultRes<?> deleteFriend(long userId2, String userIdList) {
        String[] userList = userIdList.split(",");
        for (String value : userList) {
            long userId1 = Long.parseLong(value);
            try {
                friendRequestRepository.deleteByUserId1AndUserId2(userId1, userId2);
            } catch (Exception e) {
                log.error("friendRequest delete error : " + e.getMessage());
                return DefaultRes.res(StatusCode.DELETE_ERROR, ResponseMessage.DELETE_ERROR);
            }
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.DELETE_USER);
    }

    @Transactional
    public DefaultRes<?> postFriendRequest(long userId1, String postUserIdList) {
        String[] userList = postUserIdList.split(",");
        for (String value : userList) {
            long userId2 = Long.parseLong(value);
            Optional<User> user = userRepository.findById(userId1);
            Optional<User> user2 = userRepository.findById(userId2);
            if (user.isPresent() && user2.isPresent()) {
                FriendRequest friendRequest = new FriendRequest(user.get().getUserId(), user2.get().getUserId(), user.get(), user2.get(), acceptStatus.NOT);
                friendRequest.setUser3(user.get());
                try {
                    friendRequestRepository.save(friendRequest);
                } catch (Exception e) {
                    log.error("friendRequest post error " + e.getMessage());
                    return DefaultRes.res(StatusCode.POST_ERROR, ResponseMessage.POST_ERROR);
                }
            }
        }
        return DefaultRes.res(StatusCode.OK, ResponseMessage.CREATED_FRIEND);
    }
}
