package com.sotact.taw.manager;

import com.sotact.taw.properties.OAuthProperties;
import com.sotact.taw.service.OAuthService;
import com.sotact.taw.serviceImpl.GoogleServiceImpl;
import com.sotact.taw.serviceImpl.KaKaoServiceImpl;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

@Service        // 이거 없으면?
public class OAuthManager {

    private Map<String, Class<? extends OAuthService>> oauthServiceMap;
    
    // 고유 토큰 값
    private OAuthProperties properties;

    public OAuthManager(OAuthProperties properties) {
        this.properties = properties;
        this.oauthServiceMap = new HashMap<>();
        oauthServiceMap.put(GoogleServiceImpl.provider, GoogleServiceImpl.class);
        oauthServiceMap.put(KaKaoServiceImpl.provider, KaKaoServiceImpl.class);
    }

    /**
     * OAuth 종류에 따라서 객체와 서비스 값 할당
     *
     * @param provider OAuth 종류
     * @return
     */
    public OAuthService getServiceObject(String provider) throws Exception {

        Class<? extends OAuthService> serviceClass = oauthServiceMap.get(provider);

        if(serviceClass == null){
            throw new Exception();
        }

        OAuthService service = null;

        try {
            service = serviceClass.getConstructor(OAuthProperties.class).newInstance(this.properties);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        
        return service;
    }

}
