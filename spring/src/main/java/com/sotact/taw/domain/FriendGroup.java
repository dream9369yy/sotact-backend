package com.sotact.taw.domain;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(FriendGroupPrimaryKey.class)
@SequenceGenerator(name = "MEMBER_SEQ_GENERATOR",
        sequenceName = "MEMBER_SEQ")
@Table(name = "taw_friend_group")
public class FriendGroup implements Serializable {
    /*
     * 현재 쓰이지는 않는 FriendGroup class (Spring Data JPA의 Entity타입)
     * 주석 처리 안한 이유 : interface 상속으로 해당 메소드들 주석 처리 불가
     * 그로 인해 관련 소스코드가 존재하는 domain 내부 관련한 class 파일, repository 파일 또한 주석 처리 불가
     * */

    @Id
    private Long userId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEMBER_SEQ_GENERATOR")
    private Long groupId;

    @Column(nullable = false, length = 100)
    String groupName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", referencedColumnName = "userId", nullable = false, updatable = false, insertable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    User user5;


}
