package com.sotact.taw.domain;

import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Embeddable
public class FriendGroupMemberPrimaryKey implements Serializable {
    /*
     * 현재 쓰이지는 않는 FriendGroupPrimaryKey class (FriendGroup의 복합키 형태)
     * 주석 처리 안한 이유 : interface 상속으로 해당 메소드들 주석 처리 불가
     * 그로 인해 관련 소스코드가 존재하는 domain 내부 관련한 class 파일, repository 파일 또한 주석 처리 불가
     * */

    Long userId;
    Long friendId;
    Long groupId;

    public FriendGroupMemberPrimaryKey(Long userId, Long friendId, Long groupId) {
        this.userId = userId;
        this.friendId = friendId;
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendGroupMemberPrimaryKey that = (FriendGroupMemberPrimaryKey) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(friendId, that.friendId) &&
                Objects.equals(groupId, that.groupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, friendId, groupId);
    }
}
