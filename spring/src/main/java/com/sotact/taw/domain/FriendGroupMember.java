package com.sotact.taw.domain;


import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(FriendGroupMemberPrimaryKey.class)
@Table(name = "taw_friend_group_member")
public class FriendGroupMember implements Serializable {
    /*
     * 현재 쓰이지는 않는 FriendGroupMember class (Spring Data JPA의 Entity타입)
     * 주석 처리 안한 이유 : interface 상속으로 해당 메소드들 주석 처리 불가
     * 그로 인해 관련 소스코드가 존재하는 domain 내부 관련한 class 파일, repository 파일 또한 주석 처리 불가
     * */

    @Id
    private Long userId;

    @Id
    private Long friendId;

    @Id
    private Long groupId;


    @MapsId("friendGroupPrimaryKey")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({@JoinColumn(name = "groupId", referencedColumnName = "groupId", updatable = false, insertable = false),
            @JoinColumn(name = "userId", referencedColumnName = "userId", updatable = false, insertable = false)})
    @OnDelete(action = OnDeleteAction.CASCADE)
    FriendGroup friendGroup;


    @MapsId("friendPrimaryKey")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({@JoinColumn(name = "friendId", referencedColumnName = "friendId", updatable = false, insertable = false)
            , @JoinColumn(name = "userId", referencedColumnName = "userId", updatable = false, insertable = false)})
    @OnDelete(action = OnDeleteAction.CASCADE)
    Friend friend;

}
