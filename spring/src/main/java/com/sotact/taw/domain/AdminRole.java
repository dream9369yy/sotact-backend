package com.sotact.taw.domain;

public enum AdminRole {
    SUPER_ADMIN("최고관리자"),
    ADMIN("일반관리자"),
    MANAGER("매니저");

    private String korean;

    AdminRole(String korean) {
        this.korean = korean;
    }
    public String getKorean(){
        return korean;
    }
}
