package com.sotact.taw.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString
@Table(name = "taw_user",
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = {"email", "userType"}
                )
        }
)
public class User implements Serializable {

    private static final long serialVersionUID = 2L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long userId;


    public User(String email, String nickname, String myTeam) {
        this.email = email;
        this.nickname = nickname;
        this.myTeam = myTeam;
    }

    @Builder
    public User(String password, String email, String nickname, emailCheck emailCheck, String emailCheckNumber, UserType userType) {
        this.password = password;
        this.email = email;
        this.nickname = nickname;
        this.emailCheck = emailCheck;
        this.emailCheckNumber = emailCheckNumber;
        this.userType = userType;
    }


    @Column(nullable = false, length = 128)
    String password;

    @Column(nullable = false, length = 100)
    String email;
    @Column(unique = true, nullable = false)
    String nickname;

    String profilePath;

    @Enumerated(EnumType.STRING)
    emailCheck emailCheck;

    String myTeam;

    @Column(nullable = false, length = 250)
    String emailCheckNumber;

    @Enumerated(EnumType.STRING)
    UserType userType;

    @Column(unique = true, nullable = true, length = 250)
    String oauthId;


}
