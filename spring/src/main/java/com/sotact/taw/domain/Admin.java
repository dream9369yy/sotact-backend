package com.sotact.taw.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Admin {

    @Id
    @Column(name = "admin_id")
    private String adminId;
    @Column(name = "admin_password")
    private String adminPassword;
    @Column(name = "admin_name")
    private String adminName;

    @Enumerated(EnumType.STRING)
    @Column(name = "admin_role")
    private AdminRole adminRole;

    @Column(name = "admin_token", length = 300)
    private String token;

}
