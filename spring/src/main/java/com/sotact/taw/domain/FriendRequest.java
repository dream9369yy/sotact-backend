package com.sotact.taw.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(FriendRequestPrimaryKey.class)
@Table(name = "taw_request_friend")
public class FriendRequest implements Serializable {

    @Id
    private Long userId1;

    @Id
    private Long userId2;

    @ManyToOne(fetch = FetchType.LAZY) //Proxy객체를 가지고옴
    @JoinColumn(name = "userId1", referencedColumnName = "userId", nullable = false, updatable = false, insertable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    User user3;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId2", referencedColumnName = "userId", nullable = false, updatable = false, insertable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    User user4;

    @JsonBackReference("acceptStatus")
    @Enumerated(EnumType.STRING)
    private acceptStatus acceptStatus;

}
