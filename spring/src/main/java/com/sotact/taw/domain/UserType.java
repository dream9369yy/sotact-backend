package com.sotact.taw.domain;

public enum UserType {
    TAW, KAKAO, GOOGLE;
}
