package com.sotact.taw.controller;

import com.sotact.taw.domain.User;
import com.sotact.taw.dto.DefaultRes;
import com.sotact.taw.model.TestPlainText;
import com.sotact.taw.repository.UserRepository;
import com.sotact.taw.util.ResponseMessage;
import com.sotact.taw.util.StatusCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.List;
import java.util.NoSuchElementException;

import static com.sotact.taw.dto.DefaultRes.FAIL_DEFAULT_RES;


@Slf4j
@Controller
public class testController {

    @Autowired
    private UserRepository userRepository;


    /*
    * resources 내부의 ajax.html 내부에 ajaxsend button에 관련된
    *
    * */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/test/admin/user")
    public ResponseEntity<?> getUserCount() {
        try {
            List<User> testUserList;
            try {
                testUserList = userRepository.findAll();
            } catch (NoSuchElementException e) {
                log.error("user find List: " + e.getMessage());
                return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            JSONArray jsonArray = new JSONArray();
            for (User user : testUserList) {
                JSONObject jsonObject = new JSONObject();
                String email = user.getEmail();
                String nickname = user.getNickname();
                jsonObject.put("email", email);
                jsonObject.put("nickname", nickname);
                jsonArray.put(jsonObject);
            }
            DefaultRes<?> ret = DefaultRes.res(StatusCode.OK, ResponseMessage.READ_USER, jsonArray.toString());
            return new ResponseEntity<>(ret, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * oauth 관련 테스트 페이지를 기본 페이지로 활용
    * */
    @RequestMapping("/")
    public String home() {
        return "redirect:/ajax.html";
    }

    /*
    * 비밀번호 암호화 관련 테스트 코드 작성
    * 그러나 현재 코드는 이용되지 않으므로 주석처리
    * */
    /*
    @PostMapping("rsaexample")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public boolean getRsaExample(@RequestBody TestPlainText testPlainText) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {

        System.out.println("value : " + testPlainText.getValue());
        System.out.println();
        String data = testPlainText.getValue();
        String key = "1234567812345678";
        String iv = "1234567812345678";

        Decoder decoder = Base64.getDecoder();
        byte[] encrypted1 = decoder.decode(data);

        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
        IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

        cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

        byte[] original = cipher.doFinal(encrypted1);
        String originalString = new String(original);
        System.out.println(originalString.trim());
//      System.out.println("공개키 : " + keyExample.getPublicKey());
//
//      String plainText = testPlainText.getiValue();
//      String encrypted = CipherUtil.encryptRSA(plainText, keyExample.getPublicKey());
//      System.out.println("암호화된 평문 : " + encrypted);
//      String decrypted = CipherUtil.decryptRSA(encrypted, keyExample.getPrivateKey());
//      System.out.println("복호화된 평문 : " + decrypted);
//      return decrypted.equals(plainText);
        return true;
    }*/

}
