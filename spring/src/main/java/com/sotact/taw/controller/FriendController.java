package com.sotact.taw.controller;

import com.sotact.taw.serviceImpl.FriendServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.sotact.taw.dto.DefaultRes.FAIL_DEFAULT_RES;

@Slf4j
@RestController
@RequestMapping("/friend")
@RequiredArgsConstructor
public class FriendController {


    private final FriendServiceImpl friendService;

    @CrossOrigin("*") /* cors 관련 테스트용도 */
    @GetMapping("")
    public ResponseEntity<?> getFriendList(@Param("userId") final long userId) {
        try {
            return new ResponseEntity<>(friendService.findList(userId), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> postFriend(@RequestParam("acceptStatus") final String acceptStatus, @RequestParam("fromUserId") final long userId1, @RequestParam("toUserId") final String postUserIdList) {
        try {
            return new ResponseEntity<>(friendService.postFriendRequest(acceptStatus, userId1, postUserIdList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteFriend(@RequestParam("fromUserId") final long userId1, @RequestParam("toUserId") final String userIdList) {
        try {
            return new ResponseEntity<>(friendService.deleteFriend(userId1, userIdList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
