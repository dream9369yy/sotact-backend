package com.sotact.taw.controller;

import com.sotact.taw.serviceImpl.FriendGroupServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.sotact.taw.dto.DefaultRes.FAIL_DEFAULT_RES;

@Slf4j
@RestController
@RequestMapping("/friendGroup")
@RequiredArgsConstructor
public class FriendGroupController {

    /*
    * FriendGroupController 관련 CRUD 로직
    * 현재 이용되지 않으므로 주석처리
    * */
    /*
    private final FriendGroupServiceImpl friendGroupService;

    @GetMapping("")
    public ResponseEntity<?> getFriendGroupList(@Param("userId") final long userId) {
        try {
            return new ResponseEntity<>(friendGroupService.findList(userId), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> postFriendGroup(@RequestParam("userId") final long userId, @RequestParam("groupNames") final String postGroupNameList) {
        try {
            return new ResponseEntity<>(friendGroupService.postFriendGroup(userId, postGroupNameList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteFriendGroup(@RequestParam("fromUserId") final Long userId, @RequestParam("toGroupId") final String groupIdList) {
        try {
            return new ResponseEntity<>(friendGroupService.deleteFriend(userId, groupIdList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    */
}
