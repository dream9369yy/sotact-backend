package com.sotact.taw.controller;

import com.sotact.taw.domain.Admin;
import com.sotact.taw.repository.AdminRepository;
import com.sotact.taw.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@ResponseBody
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AdminRepository adminRepository;

    @GetMapping(value = "")
    public void goToAdmin(HttpServletResponse response,@RequestParam(value = "adminToken",defaultValue = "") final String token) throws Exception {
        if(token.equals("")) response.sendRedirect("/admin/login.html");
        else response.sendRedirect("/admin/index.html");
    }


    @PostMapping("/login")
    @ResponseBody
    public Map<String,Object> testAdminLogin(@RequestBody Map<String, Object> payloads) throws IOException {
        Map<String,Object> ret = new HashMap<>();
        String adminId = (String) payloads.get("adminId");
        String adminPassword = (String) payloads.get("adminPassword");
//        String adminRole = (String) payloads.get("adminRole");
        Optional<Admin> admin = adminRepository.findByAdminIdAndAdminPassword(adminId, adminPassword);
        if(admin.isPresent()) { // 유효한 domain
            String adminToken = jwtUtil.createToken("admin", payloads);
            ret.put("adminToken",adminToken);
            ret.put("status","ok");
        }
        else {
            ret.put("status","no");
        }
        return ret;
    }
    @GetMapping(value = "/login")
    public void goToAdminLogin(HttpServletResponse response)throws Exception{
        response.sendRedirect("/admin/login.html");
    }

    @GetMapping(value = "/manage/user")
    public void goToUserManage(HttpServletResponse response,@RequestParam(value = "adminToken",defaultValue = "") final String token)throws Exception{
        if(token.equals("")) response.sendRedirect("/admin/login.html");
        else response.sendRedirect("/admin/user-manage.html");
    }
}