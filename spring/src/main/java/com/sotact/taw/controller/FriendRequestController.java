package com.sotact.taw.controller;

import com.sotact.taw.serviceImpl.FriendRequestServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.sotact.taw.dto.DefaultRes.FAIL_DEFAULT_RES;

@Slf4j
@RestController
@RequestMapping("/friendRequest")
@RequiredArgsConstructor
public class FriendRequestController {

    private final FriendRequestServiceImpl friendRequestService;

    @GetMapping("")
    public ResponseEntity<?> getFriendRequestList(@Param("userId") final long userId) {
        try {
            return new ResponseEntity<>(friendRequestService.findList(userId), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> postFriendRequest(@RequestParam("fromUserId") final long userId1, @RequestParam("toUserId") final String postUserIdList) {
        try {
            return new ResponseEntity<>(friendRequestService.postFriendRequest(userId1, postUserIdList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteFriendRequest(@RequestParam("fromUserId") final long userId1, @RequestParam("toUserId") final String userIdList) {
        try {
            return new ResponseEntity<>(friendRequestService.deleteFriend(userId1, userIdList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
