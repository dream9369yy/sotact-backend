package com.sotact.taw.controller;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@ResponseBody
@RequestMapping(value = "/download")
public class DownloadController {
    @GetMapping(value = "")
    public void goToAdmin(HttpServletResponse response) throws Exception {
        response.sendRedirect("/download/index.html");
    }
}
