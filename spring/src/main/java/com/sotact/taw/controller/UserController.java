package com.sotact.taw.controller;

import com.sotact.taw.domain.User;
import com.sotact.taw.domain.UserType;
import com.sotact.taw.domain.emailCheck;
import com.sotact.taw.dto.DefaultRes;
import com.sotact.taw.dto.UserDTO;
import com.sotact.taw.exception.EmailSendException;
import com.sotact.taw.exception.IncompleteEmailCheck;
import com.sotact.taw.exception.OAuthException;
import com.sotact.taw.exception.PasswordMismatchException;
import com.sotact.taw.manager.OAuthManager;
import com.sotact.taw.service.OAuthService;
import com.sotact.taw.service.UserService;
import com.sotact.taw.util.JwtUtil;
import com.sotact.taw.util.ResponseMessage;
import com.sotact.taw.util.StatusCode;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

import static com.sotact.taw.dto.DefaultRes.*;

/**
 * 상태 코드 참고
 * https://gist.github.com/subicura/8329759
 */
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    private final OAuthManager oauthManager;
    private final JwtUtil jwtUtil;

    /**
     * 회원가입)
     *
     * @param user          회원가입 입력값 [이메일, 비밀번호, 비밀번호 재입력, 이름]
     * @param bindingResult 검증 결과
     */
//    @Transactional(isolation = Isolation.SERIALIZABLE)
    @PostMapping(value = "/signup", consumes = "application/json", produces = "application/json")
    public ResponseEntity signUpAccount(@RequestBody @Valid UserDTO user, BindingResult bindingResult) {
        // 입력값 확인
        if (bindingResult.hasErrors()) {      // validation에 문제 발생
            List<ObjectError> errors = bindingResult.getAllErrors();
            StringBuffer err = new StringBuffer();
            for (ObjectError error : errors) {
                err.append(error.getDefaultMessage());
            }
            DefaultRes response = DefaultRes.res(StatusCode.BAD_REQUEST, err.toString());
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

//        System.out.println("현재 실행되고 있는 스레드의 이름 : " + Thread.currentThread().getName());
        // 이메일 중복 확인
        boolean emailValidate = userService.duplicateEmail(user.getEmail());
        // FIXME : 닉네임 중복 예외에 관해 이메일 중복 에러가 출력됨 -> 1차 해결
        // 닉네임 중복 확인 -> 개별적 수행
        // TODO : 소셜 로그인의 닉네임 중복에 대한 예외 처리를 요함
        boolean nicknameValidate = userService.duplicateNickname(user.getNickname());
        // 정보 등록
        if (emailValidate) {
            if(nicknameValidate){
                try {
                    userService.registerAccount(user);

                } catch (DataIntegrityViolationException de) {    // 이메일 중복 (DB에 의한 중복확인)
                    return new ResponseEntity(de.getMessage(), HttpStatus.OK);

                } catch (EmailSendException me) {
                    return new ResponseEntity(me.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

                } catch (Exception e) {
                    e.printStackTrace();
                    return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else return new ResponseEntity(DUPLICATE_NICKNAME, HttpStatus.OK);        // 닉네임 중복
        } else {    // 이메일 중복
            return new ResponseEntity(DUPLICATE_EMAIL, HttpStatus.OK);
        }
        return new ResponseEntity(SUCCESS_SIGNUP, HttpStatus.OK);
    }

    /**
     * 회원가입) 이메일 인증 완료
     *
     * @param email 회원 이메일
     * @param key   인증키
     */
    @GetMapping("/complete-signup")
    public ResponseEntity signUpComplete(@RequestParam String email, String key) {
        boolean isAuthorized = false;
        try {
            User user = userService.signUpComplete(email, key);
            emailCheck check = user.getEmailCheck();
            // 이메일 활성화
            if (check == emailCheck.OK) {
                isAuthorized = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        // session ?
        return new ResponseEntity(OK, HttpStatus.OK);
    }

    /**
     * OAuth 로그인 창으로 페이지 이동
     *
     * @param provider 사용할 OAuth
     * @param response
     */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(value = "/oauth")
    public ResponseEntity oauthLoginView(@RequestParam String provider,
                                         HttpServletResponse response) {
        try {
            OAuthService oauthService = oauthManager.getServiceObject(provider);
            System.out.println("oauthService url : " + oauthService.getRedirectUrl());
            response.sendRedirect(oauthService.getRedirectUrl());
        } catch (Exception e) {
            log.error("oauthLoginView Failed => " + e.getMessage());
            return new ResponseEntity(FAIL_DEFAULT_REQ, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * OAuth 계정 인증 후 유저 정보 확인 -> 로그인 & 회원가입 진행
     *
     * @param provider OAuth 종류
     * @param code     인증 코드
     * @return 유저 정보
     * @deprecated 변경된 OAuth 구조에 적합하지 않음
     */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(value = "/{provider}-login")
    public ResponseEntity getOAuthUserInfo(@PathVariable(value = "provider") String provider,
                                           @RequestParam(value = "code") String code) {
        User account = null;
        log.info("provider : " + provider);
        log.info("code : " + code);
        try {
            account = userService.getOAuthUserInfo(provider, code);
        } catch (OAuthException e1) {
            return new ResponseEntity<String>(e1.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            log.error("getOAuthUserInfo Failed => " + e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        DefaultRes SUCCESS_OAUTH_LOGIN = new DefaultRes(StatusCode.OK, ResponseMessage.OAUTH_LOGIN_SUCCESS, account);
        return new ResponseEntity(SUCCESS_OAUTH_LOGIN , HttpStatus.OK);
//        return new ResponseEntity<>(account.toString(), HttpStatus.OK);
    }

    @Deprecated
    @PostMapping(value = "/login/{provider}")
    public ResponseEntity oauthLogin(@RequestBody UserDTO user, @PathVariable String provider) {
        System.out.println("oauth provider : " + provider);
        System.out.println("user info : " + user.toString());
        User oauthUser = null;
        try {
            oauthUser = userService.oauthLogin(provider, user);
        } catch (Exception e) {
            log.error("oauthLogin Failed => " + e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(oauthUser.toString(), HttpStatus.OK);
    }

    /**
     * 유저 로그인
     */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
    public ResponseEntity login(@RequestBody UserDTO user) {
        User account = null;
        try {
            account = userService.checkAccount(user.getEmail(), user.getPassword(), UserType.TAW);
        } catch (IncompleteEmailCheck ie) {
            ie.getStackTrace();
            return new ResponseEntity(UNCHECKED_EMAIL, HttpStatus.OK);
        } catch (PasswordMismatchException pe) {
            pe.getStackTrace();
            return new ResponseEntity(FAIL_LOGIN, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
        // TODO : user account Session 처리?

        DefaultRes SUCCESS_LOGIN = new DefaultRes(StatusCode.OK, ResponseMessage.LOGIN_SUCCESS, account);
        return new ResponseEntity(SUCCESS_LOGIN, HttpStatus.OK);
    }


    /**
     * 유저 삭제
     *
     * @param userId 삭제할 유저의 고유키
     * @return 삭제 결과 정보
     */
    @DeleteMapping(value = "")
    public ResponseEntity deleteUser(Long userId) {
        System.out.println(userId + " 유저의 정보 삭제");
        try {
            userService.deleteUser(userId);
        } catch (IllegalArgumentException ie) {
            return new ResponseEntity(ie.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(SUCCESS_USERDEL, HttpStatus.OK);
    }

    /**
     * 검색 키워드가 포함된 닉네임, 이메일 데이터를 가진 유저 정보 출력
     * @param keyword 검색할 키워드
     * @return
     */
    @GetMapping(value = "/search")
    public ResponseEntity searchUser(@RequestParam String keyword){
        // 들어온 키워드가 [닉네임 or 이메일]    
        List<User> search = userService.findByEmailORNickname(keyword);
        // FIXME : gmail, naver 같은 도메인 단어 제외시키기
        return new ResponseEntity(search, HttpStatus.OK);
    }
}