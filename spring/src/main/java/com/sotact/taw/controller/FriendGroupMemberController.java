package com.sotact.taw.controller;

import com.sotact.taw.serviceImpl.FriendGroupMemberServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.sotact.taw.dto.DefaultRes.FAIL_DEFAULT_RES;

@Slf4j
@RestController
@RequestMapping("/friendGroupMember")
@RequiredArgsConstructor
public class FriendGroupMemberController {

    /*
    * FriendGroupMemberController 관련 CRUD 로직
    * 현재 이용되지 않으므로 주석처리
    * */

    /*
    @Autowired
    private FriendGroupMemberServiceImpl friendGroupMemberService;

    @GetMapping("")
    public ResponseEntity<?> getFriendGroupMemberList(@Param("userId") final long userId) {
        try {
            return new ResponseEntity<>(friendGroupMemberService.findList(userId), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> postFriendGroupMember(@RequestParam("fromUserId") final long userId, @RequestParam("groupId") final long groupId,@RequestParam("toFriendId") final String friendGroupMemberList) {
        try {
            return new ResponseEntity<>(friendGroupMemberService.postFriendGroupMember(userId, groupId,friendGroupMemberList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteFriendGroupMember(@RequestParam("fromUserId") final long userId, @RequestParam("groupId") final long groupId,@RequestParam("toFriendId") final String friendGroupMemberList) {
        try {
            return new ResponseEntity<>(friendGroupMemberService.deleteFriendGroupMember(userId, groupId,friendGroupMemberList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(FAIL_DEFAULT_RES, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    */
}
