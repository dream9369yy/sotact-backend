package com.sotact.taw.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.sotact.taw.domain.User;
import com.sotact.taw.domain.UserType;
import com.sotact.taw.dto.UserDTO;
import com.sotact.taw.exception.*;

import java.util.List;

public interface UserService {

    boolean registerAccount(UserDTO user) throws EmailSendException;

    void sendEmailKey(String email, String emailRandKey) throws EmailSendException;

    User signUpComplete(String email, String key);

    boolean duplicateEmail(String email);

    boolean duplicateNickname(String nickname);

    @Deprecated
    User getOAuthUserInfo(String provider, String code) throws Exception, OAuthException;

    User checkAccount(String email, String password, UserType userType) throws PasswordMismatchException, IncompleteEmailCheck;

    User oauthLogin(String provider, UserDTO user) throws Exception;

    void deleteUser(Long userId);

    List<User> findByEmailORNickname(String keyword);
}
