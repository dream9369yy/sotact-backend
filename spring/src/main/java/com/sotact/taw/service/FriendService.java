package com.sotact.taw.service;

import com.sotact.taw.dto.DefaultRes;
import org.springframework.boot.configurationprocessor.json.JSONException;

public interface FriendService {
    DefaultRes<?> findList(long userId) throws JSONException;

    DefaultRes<?> deleteFriend(long userId,String userIdList);
}
