package com.sotact.taw.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.sotact.taw.domain.User;
import com.sotact.taw.dto.UserDTO;

public interface OAuthService {
    String getRedirectUrl();

    String getAccessToken(String code);

    JsonNode getOAuthUserInfo(String accessToken);

    User saveOAuthUserInfo(JsonNode user);
    User saveOAuthUserInfo(UserDTO user);
}
