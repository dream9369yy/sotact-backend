package com.sotact.taw.util;

public class ResponseMessage {
    public static final String OK = "성공";
    public static final String LOGIN_SUCCESS = "로그인 성공";
    public static final String OAUTH_LOGIN_SUCCESS = "로그인 성공";
    public static final String LOGIN_FAIL = "로그인 실패";
    public static final String READ_USER = "회원 정보 조회 성공";
    public static final String NOT_FOUND_USER = "회원을 찾을 수 없습니다.";
    public static final String CREATED_USER = "회원 가입 성공";
    public static final String FAIL_CREATE_USER = "회원 가입 실패";
    public static final String UPDATE_USER = "회원 정보 수정 성공";
    public static final String DELETE_USER = "회원 탈퇴 성공";
    public static final String INTERNAL_SERVER_ERROR = "서버 내부 에러";
    public static final String DB_ERROR = "데이터베이스 에러";
    public static final String POST_ERROR = "게시물 등록 에러";
    public static final String LIKE_ERROR= "좋아요 IP 중복 에러";
    public static final String DELETE_ERROR= "삭제 에러";
    public static final String CREATED_FRIEND = "친구를 등록하였습니다";
    public static final String DELETE_FRIEND = "친구를 삭제하였습니다";
    public static final String LIST_FRIEND = "친구를 조회하였습니다";
    public static final String DENY_FRIEND= "친구 수락을 거절하였습니다";
    public static final String REQUEST_ERROR = "요청 에러";
    public static final String EMAIL_ERROR = "이메일 전송 에러";
    public static final String DUPLICATE_EMAIL = "이미 존재하는 이메일입니다.";
    public static final String DUPLICATE_NICKNAME = "이미 존재하는 닉네임입니다.";
    public static final String UNCHECKED_EMAIL = "이메일 인증이 완료되지 않았습니다.";
    public static final String FAIL_DELETE_USER = "can't delete user. user is not existed.";
    public static final String USERDEL_SUCCESS = "delete user successfully";
    public static final String SUCCESS_FOUND_USER = "유저 검색 완료";
}
