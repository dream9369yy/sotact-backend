package com.sotact.taw.util;

import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

@Component
public class KeyExample {
    private final PrivateKey privateKey;
    private final PublicKey publicKey;

    public KeyExample() throws NoSuchAlgorithmException {
        KeyPair keyPair = CipherUtil.genRSAKeyPair();
        this.privateKey = keyPair.getPrivate();
        this.publicKey = keyPair.getPublic();
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }
}
