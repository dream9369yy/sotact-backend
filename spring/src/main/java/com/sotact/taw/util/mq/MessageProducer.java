package com.sotact.taw.util.mq;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private TopicExchange topic;

    public void sendMessage(String routingKey, String text){
        rabbitTemplate.convertAndSend(topic.getName(), routingKey, text);
        System.out.println("sendMessage execute");

    }

}
