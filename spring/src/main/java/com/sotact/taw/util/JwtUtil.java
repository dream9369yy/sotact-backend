package com.sotact.taw.util;

import com.sotact.taw.exception.UnauthorizedJwtException;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class JwtUtil {

    // 검증용
    private static final String SECRET_KEY = "salt_temp";

    /**
     * 토큰 생성
     */
    public static String createToken(String key, Map<String, Object> payloads) {
        // JWT 토큰 헤더
        Map<String, Object> headers = new HashMap();
        headers.put("typ", "JWT");
        headers.put("alg", "HS256");     // HMAC SHA256

        long now = System.currentTimeMillis();

        return Jwts.builder()
                .setHeader(headers)
                .claim(key, payloads)
                .setExpiration(new Date(now + 10000000))    // FIXME 유효시간 temp
                .setIssuedAt(new Date(now))
                .signWith(SignatureAlgorithm.HS256, convertToBytes(SECRET_KEY))
                .compact();
    }

    /**
     * 인코딩/디코딩 과정에 필요한 바이트 배열로 변환
     * charset 변동 중 정보 손실을 막기 위함
     */
    public static byte[] convertToBytes(String secret) {
        byte[] key = null;
        try {
            key = secret.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return key;
    }

    /**
     * 유효한 JWT 토큰인지 확인
     */
    public boolean isValid(String jwt) throws UnauthorizedJwtException {
        try {

            Jws<Claims> claims = Jwts.parser()
                    .setSigningKey(convertToBytes(SECRET_KEY))
                    .parseClaimsJws(jwt);
            return true;
        } catch (Exception e) {
            // 서명 확인 에러 : SignatureException
            throw new UnauthorizedJwtException();
        }
    }

    /**
     * 토큰 파싱 & 검증
     */
    public Map<String, Object> getToken(String jwt, String key) {
        Jws<Claims> claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(this.convertToBytes(SECRET_KEY))
                    .parseClaimsJws(jwt);
        } catch (ExpiredJwtException e1) {
            // 만료 예외 ExpiredJwtException
            // TODO : 만료 토큰 리프레시 처리
            System.out.println(e1.getClaims());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (LinkedHashMap<String, Object>) claims.getBody().get(key);
    }
}


/*

  추가해야 할 사항 [ 참고 ]

  https://alwayspr.tistory.com/8
  리프레시
  https://velog.io/@tlatldms/%EC%84%9C%EB%B2%84%EA%B0%9C%EB%B0%9C%EC%BA%A0%ED%94%84-Spring-boot-Spring-security-JWT-Redis-mySQL-3%ED%8E%B8-kkk5mdtnzx


//1) ExpiredJwtException : JWT를 생성할 때 지정한 유효기간 초과할 때.
//2) UnsupportedJwtException : 예상하는 형식과 일치하지 않는 특정 형식이나 구성의 JWT일 때
//3) MalformedJwtException : JWT가 올바르게 구성되지 않았을 때
//4) SignatureException :  JWT의 기존 서명을 확인하지 못했을 때
//5) IllegalArgumentException



*/