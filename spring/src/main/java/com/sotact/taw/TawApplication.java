package com.sotact.taw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TawApplication {

    public static void main(String[] args) {
        SpringApplication.run(TawApplication.class, args);
/*
        String os = System.getProperty("os.name").toLowerCase().toLowerCase();
        // sh명령어 vs bash 명령어
        ProcessBuilder processBuilder;
        if (isWindows(os)) {
            processBuilder = new ProcessBuilder("cmd.exe");
        } else {
            processBuilder = new ProcessBuilder("bash", "-c", "npm start");
        }
        processBuilder.redirectErrorStream(true);
        processBuilder.directory(new File("../nodejs"));
        Process process = processBuilder.start();
        InputStream inputStream = process.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        */
    }

    /*
    private static boolean isWindows(String os) {
        return os.contains("win");
    }
    */
}
