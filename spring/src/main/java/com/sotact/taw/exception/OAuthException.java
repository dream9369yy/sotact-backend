package com.sotact.taw.exception;

public class OAuthException extends Throwable {
    public OAuthException(String message) {
        super(message);
    }

}
