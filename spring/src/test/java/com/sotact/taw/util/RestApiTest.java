package com.sotact.taw.util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApiTest {

    @Test
    public void getFromRestApiAndDecode() throws ParseException {
        RestTemplate rt = new RestTemplate();
        String localUrl = "http://0.0.0.0:8080/friend?userId=2";
        ResponseEntity<String> resEntity = rt.getForEntity(localUrl, String.class);

        JSONParser jsonParser = new JSONParser();
        JSONObject parse = (JSONObject) jsonParser.parse(resEntity.getBody());
        String data = (String) parse.get("data");
        JSONArray jsonArray = (JSONArray) jsonParser.parse(data);
        for (Object object2 : (JSONArray) jsonArray.get(0)) {
            JSONObject jsonObject = (JSONObject) object2;
            long friendId = (long) jsonObject.get("friendId");
            String nickname = (String) jsonObject.get("nickname");
            System.out.println("friendId : " + friendId + " nickname : " + nickname);
        }
        for (Object object2 : (JSONArray) jsonArray.get(1)) {
            JSONObject jsonObject = (JSONObject) object2;
            long groupId = (long) jsonObject.get("groupId");
            String groupName = (String) jsonObject.get("groupName");
            System.out.println("groupId : " + groupId + " groupName : " + groupName);
        }

        for (Object object2 : (JSONArray) jsonArray.get(2)) {
            JSONObject jsonObject = (JSONObject) object2;
            long friendId = (long) jsonObject.get("friendId");
            long groupId = (long) jsonObject.get("groupId");
            System.out.println("friendId : " + friendId + " groupId : " + groupId);
        }

    }
}
