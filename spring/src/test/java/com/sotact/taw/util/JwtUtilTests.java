package com.sotact.taw.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sotact.taw.domain.Admin;
import com.sotact.taw.domain.AdminRole;
import com.sotact.taw.domain.User;
import com.sotact.taw.repository.AdminRepository;
import com.sotact.taw.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@RequiredArgsConstructor
public class JwtUtilTests {

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    private AdminRepository adminRepository;

    public User sampleUser(){
        User sample = new User();

        sample.setEmail("test email");
        sample.setUserId(11L);
        sample.setPassword("1234567");
        return sample;
    }

    public Admin sampleAdmin() {
        Admin admin = new Admin();
        admin.setAdminId("gktgnjftm");
        admin.setAdminPassword("dkqrnjs2");
        admin.setAdminRole(AdminRole.ADMIN);
        admin.setAdminName("굿굿");
        return admin;
    }
    @Test
    public void create(){

        ObjectMapper mapper = new ObjectMapper();
//        Map<String, Object> payloads = mapper.convertValue(sampleUser(), Map.class);
        Admin admin = sampleAdmin();
        Map<String,Object> payloads = mapper.convertValue(admin,Map.class);
//        System.out.println(payloads);

        String result = jwtUtil.createToken("user",payloads);
        admin.setToken(result);
        System.out.println("결과 : \n"+result);
        adminRepository.save(admin);
        try {

            System.out.println((jwtUtil.isValid(result))?"값같음":"값달라");
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("값이 다름");
        }

        try {
            jwtUtil.isValid(result+"sdfsd");
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("값이 다름");
        }

    }

    @Test
    public void parse(){

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> payloads = mapper.convertValue(sampleUser(), Map.class);

        String result = jwtUtil.createToken("user",payloads);
        System.out.println("복호화 :");
        jwtUtil.getToken(result,  "user");
        System.out.println(jwtUtil.getToken(result,  "user"));
    }
}
