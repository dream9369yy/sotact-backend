package com.sotact.taw.util;

import com.sotact.taw.domain.*;
import com.sotact.taw.repository.*;
import com.sotact.taw.util.SHA256Encryption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DBTest {
    @Autowired
    private FriendRepository friendRepository;
    @Autowired
    private FriendRequestRepository friendRequestRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FriendGroupMemberRepository friendGroupMemberRepository;
    @Autowired
    private FriendGroupRepository friendGroupRepository;

    @Autowired
    private SHA256Encryption sha256Encryption;

    @Test
    public void friendRequestTest() {
        // friendRequestTable에 insert -> OK
        for (long i = 1; i <= 100; i++) {
            Optional<User> user = userRepository.findById(700L);
            Optional<User> user2 = userRepository.findById(i + 1);
            if (user.isPresent() && user2.isPresent()) {
                FriendRequest friendRequest = new FriendRequest(700L,i+1, user.get(),user2.get(), acceptStatus.NOT);
                friendRequest.setUser3(user.get());
                friendRequestRepository.save(friendRequest);
            }
        }


        //friendRequest테이블만 온전히 지우기 -> OK
        /*
        for (long i = 1; i <= 12; i++) {
            FriendRequestPrimaryKey friendRequestPrimaryKey = new FriendRequestPrimaryKey(i, i + 1);
            Optional<FriendRequest> friendRequest = friendRequestRepository.findById(friendRequestPrimaryKey);
            friendRequest.ifPresent(request -> friendRequestRepository.delete(request));
        }
        */
    }


    @Test
    public void friendGroupMemberTest() {
        //friendGroupMember table에 insert
        for (long i = 1; i <= 100; i++) {
            Optional<User> user = userRepository.findById(i);//그룹에 추가하는 유저
            Optional<User> user2 = userRepository.findById(700L); // 그룹에 추가당하는 유저
            Optional<FriendGroup> friendGroup = Optional.ofNullable(friendGroupRepository.findByGroupId(i));
            Optional<Friend> friend = friendRepository.findByUserIdAndFriendId(i, 700L);
            if (user.isPresent() && user2.isPresent() && friendGroup.isPresent() && friend.isPresent()) {
                FriendGroupMember friendGroupMember = new FriendGroupMember(i,700L,i,friendGroup.get(),friend.get());
                friendGroupMemberRepository.save(friendGroupMember);
            }
        }

        /*
         * userId로 우선 삭제 테스트(순전히 friendGroupMember테이블만 삭제를 해야함)
         * */
/*        for(long i=1;i<=5;i++) {
            Optional<User> user = userRepository.findById(i);
            friendGroupMemberRepository.deleteByFriendGroupMemberPrimaryKeyUserId(user.get().getUserId());
        }*/


    }

    @Test
    public void encryptPasswordTest() {
        String s = "test1234";
        String encrypted = sha256Encryption.encode(s);
        User user = new User(encrypted, "encrypted", "encrypttest", emailCheck.NOT, "adsfasdf",UserType.TAW);
        userRepository.save(user);
        String encrypted2 = sha256Encryption.encode(s);
        Optional<User> user2 = userRepository.findByPassword(encrypted2);
        if (user.getPassword().equals(user2.get().getPassword())) {
            System.out.println("암호화 인증 후 정확히 찾기 성공");
        } else System.out.println("실패");
    }

    @Test
    public void friendGroupTest() {
/*        for (long i = 1; i <= 100; i++) {
            Optional<User> user = userRepository.findById(i);
            if (user.isPresent()) {
                FriendGroup friendGroup = new FriendGroup();
                friendGroup.setGroupName("테스트" + i);
                friendGroup.setUserId(i);
                friendGroup.setUser5(user.get());
                friendGroupRepository.save(friendGroup);
            }
        }*/

        for(long i=13;i<=15;i++) {
            friendGroupRepository.deleteByUserId(i);
        }
    }

    @Test
    public void friendTest() {
        // user테이블에서 찾아서 친구테이블에 넣고, 이미 존재하면 insert 예외처리 -> OK
        for (long i = 1; i <= 1000; i++) {
            Optional<User> user = userRepository.findById(700L);
            Optional<User> user2 = userRepository.findById(i + 1);
            FriendPrimaryKey friendPrimaryKey = new FriendPrimaryKey(700, i + 1);
            FriendPrimaryKey friendPrimaryKey2 = new FriendPrimaryKey(i + 1, 700);
            Optional<Friend> currentFriend = friendRepository.findById(friendPrimaryKey);
            Optional<Friend> currentFriend2 = friendRepository.findById(friendPrimaryKey2);
            if (currentFriend.isPresent() && currentFriend2.isPresent()) {
                System.out.println("이미 존재하는 친구목록입니다. 다시 입력해주세요");
                continue;
            }
//            List<FriendGroupMember> friendGroupMembers2 = new ArrayList<>();
            if (user.isPresent() && user2.isPresent()) {
                User user1 = user.get();
                User user3 = user2.get();
                Friend friend = new Friend(700L,i+1, user1,user3);
                Friend friend1 = new Friend(i+1,700L, user3,user1);
                friend.setUser(user1);
                friend1.setUser(user3);
                friendRepository.save(friend);
                friendRepository.save(friend1);
            }
        }

        // 친구관계 하나씩 찾아서 지우기
/*        for (long i =16; i <= 20; i++) {
            FriendPrimaryKey friendPrimaryKey = new FriendPrimaryKey(i, 700L);
            FriendPrimaryKey friendPrimaryKey1 = new FriendPrimaryKey(700L,i);
            Optional<Friend> friend = friendRepository.findById(friendPrimaryKey);
            Optional<Friend> friend1 = friendRepository.findById(friendPrimaryKey1);
            if (friend.isPresent()) {
                friendRepository.deleteByUserIdAndAndFriendId(i,700L);
            }
            if(friend1.isPresent()) {
                friendRepository.deleteByUserIdAndAndFriendId(700L,i);
            }
        }*/
    }

    @Test
    public void userTest() {

//         user저장
        for (long i = 7; i <= 1000; i++) {
            User user = User.builder().email("dfasdf" + i)
                    .password("asdfasdf" + i)
                    .nickname("gktgnjftm" + i)
                    .emailCheck(emailCheck.NOT)
                    .password(sha256Encryption.encode("ersdfsdf"))
                    .emailCheckNumber("adfasdfasdf")
                    .build();
            userRepository.save(user);
        }

        /*
         * 유저테이블에서 삭제후 cascade옵션으로 friend테이블도 정확히 삭제되는지 확인
         * friendGroupMember Table이 연관을 받아서 그런지, root(User) Table부터 지우면 Null Error가 난다
         * 그래서, 먼저 friend테이블을 지운후에 user테이블을 지워서 하는 방법으로 선택했다
         * */
        // OK
/*        for (long i = 21; i <= 30; i++) {
            if (userRepository.findById(i).isPresent()) userRepository.deleteById(i);
        }*/


    }

}
