package com.sotact.taw.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StopWatch;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
public class MockTest {
    @Autowired
    MockMvc mockMvc;
    ExecutorService es = Executors.newFixedThreadPool(1000);
    CyclicBarrier barrier = new CyclicBarrier(1001);
    static AtomicInteger counter = new AtomicInteger(0);
    static AtomicInteger count = new AtomicInteger(0);

    @Test
    public void mockTest() throws InterruptedException, BrokenBarrierException {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            es.submit(() -> {
                int idx = counter.addAndGet(1);
                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }

                log.info("Thread " + idx);
                StopWatch stopWatch = new StopWatch();
                stopWatch.start();
                try {
                    mockMvc.perform(get("/friend?userId=700"))
                            .andReturn();
                    count.addAndGet(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log.info("{} {}", idx, stopWatch.getTotalTimeMillis());
            });
        }
        barrier.await();

        es.shutdown();
        es.awaitTermination(1000, TimeUnit.SECONDS);
        long end = System.currentTimeMillis();
        System.out.println("실행 시간 : " + (end - start) / 1000.0);
        System.out.println("응답받은 총 request 수 : " + count.get());
    }

}
