#### Welcome to our Sotact-Backend Admin-Page!!

## Layout


#### Login page

![Loginpage](assets/image/sotact-backend-admin-login.png)

#### if you don't have any admin address or password, you can't access to admin page

![LoginFailure](assets/image/sotact-backend-admin-login-failure.png)

#### This is admin page, and we plan to add features more!!

![userManage](assets/image/sotact-backend-admin-usermanage.png)

## Document


First of all, Our Deploy Domain name is [http://jpa.esllo.com/](http://jpa.esllo.com/) 

but if you want to access admin page in localhost, [http://localhost:8080/admin](http://localhost:8080/admin) (default server port is 8080, but if you want to use another port, [http://localhost:port/admin](http://localhost:8080/admin) , port is number

### Login

---

```
url : http://localhost:port/admin/login
method : POST
Accept : application/json
Content-Type : application/json

Required : adminId, and adminPassword as json type

{
  "adminId":"test",
  "adminPassword" :"test"
}

```

### Success Response

---

```
StatusCode : 200
Content : you are redirecting to index.html, so contents are index.html
```

### Failed Response

---

```
StatusCode : 200
Content :
{
    "status": "no"
}
```