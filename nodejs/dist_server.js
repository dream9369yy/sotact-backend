const http = require('http');
const fs = require('fs');
const url = require('url');
const io = require('socket.io');

const level = require('level');
var path = require('path');
const dbPath = path.join(__dirname,'mydb')
let db = level(dbPath)

if (!fs.existsSync('./data')) {
  fs.mkdirSync('data');
}

function getData(key) {
  let files = fs.readdirSync('./data');
  let index = files.indexOf(key);
  if (index == -1) {
    return null;
  }
  let data = fs.readFileSync('./data/' + files[index], 'utf8');
  return data;
}

function saveData(key, data) {
  if (typeof data == 'object')
    data = JSON.stringify(data);
  let result = fs.writeFileSync('./data/' + key, data, 'utf8');
  console.log(result);
}
makeKey();
// 구분에 필요한 키 설정 
function makeKey(){
  let key = Math.random().toString(36).substr(2,11);
  console.log("make a new room key : ", key);
  // let room_info = {'key':key, 'user_id':'user1'}; 유저 
  db.put('key', key, function (err) {
    if(err) {
        console.log(err);
    }
    db.get('key', function (err, value) {
        if (err) {
            console.log(err);
        }
        console.log(value);
        // TODO : send client
    });
  });
}

const room = new Map();
const reqRoom = {};
const reqRoomTm = {};
const roomWrapper = {};

function checkRoom(key) {
  return function () {
    if (room.get(key) != undefined) {
      if (reqRoom[key] != undefined) {
        reqRoom[key].forEach(res => res.end(JSON.stringify({
          result: true,
          dist: room.get(key)
        })));
      }
    }
  }
}

function createOrPush(target, key, value) {
  if (target[key] == undefined)
    target[key] = [value];
  else
    target[key].push(value);
}

const server = http.createServer((req, res) => {
  var body = '';
  req.on('data', (data) => {
    body += data;
  });
  req.on('end', () => {
    res.writeHead(200, {
      'Content-Type': 'application/json; charset=utf-8',
    });
    try {
      let data = JSON.parse(body);
      if (data.action == "load") {
        let d = getData(data.key);
        if (d == null) throw 'Key Not Exists';
        res.end(JSON.stringify({ result: true, action: data.action, response: d }));
      } else if (data.action == "save") {
        if (data.json == undefined) throw 'Data Not Exists';
        saveData(data.key, data.json);
        res.end(JSON.stringify({ result: true, action: data.action }));
      } else if (data.action == "room") {
        if (room.get(data.key) != undefined) {
          res.end(JSON.stringify({ result: true, dist: room.get(data.key) }));
        } else {
          let client = getPrefClient();
          let d = getData(data.key);
          if (client == undefined)
            throw 'Server Not Exists';
          console.log('open room..');
          client.emit('openRoom', { key: data.key, data: d });
          createOrPush(reqRoom, data.key, res);

          if (reqRoomTm[data.key] != null)
            clearTimeout(reqRoomTm[data.key]);
          reqRoomTm[data.key] = setTimeout(checkRoom(data.key), 2000);
        }
      } else {
        throw 'Unknown Action';
      }
    } catch (e) {
      res.end(JSON.stringify({ result: false, message: e }));
    }
  });
});
server.listen(5500);
const socket = io.listen(3001);
const clients = [];
function getPrefClient() {
  checkClients();
  return clients[0];
}

function checkClients() {
  let removed = 0;
  for (let i = 0; i < clients.length - removed; i++) {
    if (clients[i].disconnected) {
      if (roomWrapper[clients[i]] != undefined)
        roomWrapper[clients[i]].forEach(key => room.delete(key));
      delete roomWrapper[clients[i]];
      clients.splice(i--, 1);
    }
  }
  console.log(`Available Clients : ${clients.length}`);
}

socket.on('connection', client => {
  clients.push(client);
  console.log('New Client Connected');
  checkClients();
  client.on('roomOpened', data => {
    console.log('room opened..');
    room.set(data.room, data);
    createOrPush(roomWrapper, client, data.room);
    if (reqRoom[data.room] != undefined)
      reqRoom[data.room].forEach(res => res.end(JSON.stringify({
        result: true,
        dist: data
      })));
    delete reqRoom[data.room];
  });
  client.on('disconnect', () => {
    console.log('Client Disconnected');
    checkClients();
  });
});