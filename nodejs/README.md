# Sotact-Backend for NodeJS

## Introduction
다른 사용자들간의 협업을 위해 존재하는 미디어 서버입니다.  
소켓 통신을 통해 실시간으로 변경되는 애니메이트 작업 메타 데이터를 공유해 전달을 합니다.  


### Architecture

---

![architecture](../spring/assets/image/sotact-backend-architecture.png)

### Version

---
|Project Environment|Version|
|:---:|:---:|
|`npm`|6.14.7|   
|`nodejs`|v10.19.0|

### Must Be Set in Advance when Deploying NodeJS Server

---
For CentOS : install npm  
(It is a Standard : Centos 8.2, package manager : yum)  
```bash
sudo yum install npm
npm start
```
For Ubuntu: install npm   
(Ubuntu 20.04, package manager : apt)  
```bash
sudo apt install npm
npm start
```

For Mac OS : install npm
```bash
brew install npm
npm start
```


but default port is 3000, so it won't start if you use port 3000 already.  
so if you want to use another port, go to index.js file and then modify below code  
```javascript
var app = http.createServer(function(req, res) {
    fileServer.serve(req, res);
}).listen(3000); /* modify port number : 3000*/
```  

### For Deploy

---

```bash
git clone https://git.swmgit.org/swmaestro/sotact-backend.git
cd sotact-backend
git checkout develop # if master branch is empty
```

For NodeJS: run these executions  
(For example : run io_server.js file)
```bash
cd nodejs
npm install
node io_server
```

**default port is 3000**  
  
### Furthermore

if you are interested in monitoring system, follow below these steps  

First, install prom-client (from npm package) 
```bash
npm install prom-client
```
Second, install prometheus and grafana

Reference : [Spring Boot README.md](../spring/README.md)

Third, set these codes in js file, in detail , use prom-client and set prefix name  
and then mapping default url (in this example url name is metrics) 
```javascript
const client = require('prom-client')
client.collectDefaultMetrics({prefix:'sotact_io_server'})


    if(req.url === "/metrics") {
        res.setHeader('Content-Type',client.register.contentType)
        res.end(client.register.metrics())
    }
```

in prometheus /etc/prometheus/prometheus.yml (in Ubuntu 20.04)
```bash
scrape_configs:
  - job_name: "io_server node"
    metrics_path: "/metrics" # Application prometheus endpoint
    static_configs:
      - targets: ['localhost:4000'] # Application host:port
```

---

### Sotact-BackEnd Contributors

---

구연수 : kbeeys@gmail.com

이윤석 : gktgnjftm@naver.com
