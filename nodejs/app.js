var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var upload = require('./AWS/upload')
var download = require('./AWS/download')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// https://localhost:3000/upload -> S3에 파일 업로드
app.get('/upload',(req,res)=>{
  upload('./credit-card-template.psd')
})
//https://localhost:3000/download -> S3로부터 파일 다운로드
app.get('/download',(req,res)=>{
  download('public/images/다운로드테스트3.psd') // 상대경로 지정가능
})

app.get('/:room',(req,res)=>{
  console.log('room name is :'+req.params.room)
  res.render('socketChattingSample',{room:req.params.room})
})
// konva.js 테스트용 
app.get('/canvas/:room', (req, res)=>{
  res.render('sharedObject.ejs',{room:req.params.room})
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
