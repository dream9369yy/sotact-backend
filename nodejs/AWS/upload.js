const fs = require('fs')
const AWS = require('aws-sdk')
const bucket = ''
const {v4: uuidv4} = require('uuid');
const s3 = new AWS.S3({
    accessKeyId: "", secretAccessKey: ""
    , region: ''
})
const uploadFile = (fileName, Name, type, res, db, has) => {
    const fileContent = fs.readFileSync(fileName)
    let S3uploadName = uuidv4()
    const params = {
        Bucket: bucket,
        Key: S3uploadName + '.' + type,
        Body: fileContent
    }

    const dbValue = {
        who: has,
        original: Name,
        hash: S3uploadName
    }
    let a= {
        who : has,
        original : Name,
        share : ""
    }
    s3.upload(params, async (err, data) => {
        if (err) {
            console.log('S3 upload error : ', err)
            res.end(JSON.stringify({res: 'S3 Upload error'}))
            throw err
        } else {
            console.log(`File Uploaded Successfully. ${data.Location}`)

            // S3 파일이 있는지 없는지 체크
            db.put(S3uploadName, JSON.stringify(a), (err) => {
                if (err) {
                    console.log('S3uploadName LevelDB err')
                    console.log(err)
                }
            })
            db.get(has, (err, data) => {
                if (err) { //키가 없는 에러
                    console.log('there has no key ', err)
                }
                if (data === undefined) {
                    db.put(has, JSON.stringify(dbValue), (err) => {
                        if (err) {
                            console.log('LevelDB put error : ' + err)
                            throw err
                        }
                        console.log(dbValue)
                    })
                } else {
                    console.log(dbValue)
                    data += ';'
                    data += JSON.stringify(dbValue)
                    db.put(has, data, (err) => {
                        if (err) {
                            console.log('LevelDB put error : ' + err)
                            throw err
                        }
                    })
                }
            })

            res.end(JSON.stringify({res: 'S3 Upload OK'}))
        }
    })
}

const deleteObject = (Name, db, has, res) => {
    const params = {
        Bucket: bucket,
        Key: Name + '.' + 'taw'
    }
    s3.deleteObject(params, async (err) => {
        if (err) {
            // delete를 해도 marker가 삽입되어서 err가 실제로는 동작하지 않음.. , 현재 Research중..
            console.log('S3 has no Object error')
            console.log(err)
            res.end(JSON.stringify('there has no files from S3 Storage'))
        } else {
            db.get(has, (err, data1) => {
                if (err) {
                    console.log('there has no key')
                    console.log('이미 지워졌거나, 없는 파일입니다.')
                    res.end(JSON.stringify('there has no files from S3 Storage'))
                } else {
                    console.log('client has file list')
                    console.log(data1)
                    db.get(Name, (err, data2) => {
                        if (err) {
                            console.log('이미 지워졌거나, 없는 파일입니다')
                            res.end(JSON.stringify('there has no files from S3 Storage'))
                        } else {
                            console.log('file has or not')
                            console.log(data2)
                            db.del(Name, (err) => {if (err) {
                                    console.log('levelDB file Deletion Error')
                                }
                            })
                            db.get(has, (err, data) => {
                                console.log('leveldb client has file list')
                                console.log(data1)
                                let newData = ""
                                data1.split(';').forEach(e => {
                                    const object = JSON.parse(e)
                                    if (object.hash !== Name) {
                                        newData += e
                                        newData += ';'
                                    }
                                })
                                newData = newData.substring(0, newData.length - 1)
                                console.log(newData)
                                if (newData === "") {
                                    db.del(has, (err) => {
                                        if (err) {
                                            console.log('levelDB key Deletion Error')
                                        } else {
                                            console.log('levelDB key Deletion Success')
                                            res.end(JSON.stringify('Delete file OK'))
                                        }
                                    })
                                } else {
                                    db.put(has, newData, (err) => {
                                        if (err) {
                                            console.log(err)
                                        } else {
                                            console.log('update levelDB Value OK')
                                            res.end(JSON.stringify('Delete file OK'))
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}


const updateFile = (fileName,hashName,Type,res,db) => {
    const fileContent = fs.readFileSync(fileName)
    const params = {
        Bucket: bucket,
        Key: hashName + '.' + Type,
        Body: fileContent
    }
    s3.upload(params,async(err,data)=>{
        if(err) {
            console.log('S3 Save Update Error ',err)
            res.end(JSON.stringify('S3 Save Update Error'))
        }
        else {
            console.log(`Update File Successfully. ${data.Location}`)
        }
    })
}
module.exports = {uploadFile, deleteObject,updateFile}