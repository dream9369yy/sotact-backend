const ios = require('socket.io');
const ioc = require('socket.io-client');
const { inherits } = require('util');

let port = 3002;
let server = null;

check(false);
function check(b) {
  if (b) {
    port--;
    console.log('listen on :' + port);
    server = ios.listen(port);
    server.on('connection', socket => {
      console.log('New Client Connected *');
    })

  } else {
    portAvailable(port++, check);
  }
}

function portAvailable(port, fn) {
  var net = require('net')
  var tester = net.createServer()
    .once('error', function (err) {
      fn(false)
    })
    .once('listening', function () {
      tester.once('close', function () { fn(true) })
        .close()
    })
    .listen(port)
}


const client = ioc('http://localhost:3001', { reconnection: false, autoConnect: false });
client.on('connect', () => {
  console.log('Server Connected');
});
client.on('openRoom', (pk) => {
  console.log('create room..');
  let room = server.of('/' + pk.key);
  room.on('connection', socket => {
    console.log('New Client Connected ' + room);
    initRoom(pk, room);
    bindRoom(socket);
  });
  client.emit('roomOpened', { room: pk.key, addr: 'http://localhost', port: 3002 });
});
client.connect();

const obj = new Map();
function initRoom(pk, room) {
  if (obj.has(pk.key)) return;
  console.log('New Room Initialized #' + pk.key);
  if (pk.data == null)
    pk.data = [];
  obj.set(pk.key, { room: room, data: pk.data, locks: {} });
}
function bindRoom(socket) {
  const key = socket.nsp.name.substr(1);
  console.log('New Socket binded #' + key);
  socket.on('requestLock', (pk) => {
    let rd = obj.get(key);
    if (rd.locks.hasOwnProperty(pk.src)) {
      console.log(`Lock Denied ${socket.id} - ${pk.src}`);
      socket.emit('lockDeny', pk.src);
    } else {
      console.log(`Lock Allowed ${socket.id} - ${pk.src}`);
      rd.locks[pk.src] = socket.id;
      rd.room.emit('lock', { id: pk.src, to: socket.id });
    }
  });
  socket.on('releaseLock', (pk) => {
    let rd = obj.get(key);
    if (rd.locks.hasOwnProperty(pk.src) && rd.locks[pk.src] == socket.id) {
      console.log(`Lock Released ${socket.id} - ${pk.src}`);
      delete rd.locks[pk.src];
      rd.room.emit('unlock', { id: pk.src });
    }
  });
  socket.on('requestData', () => {
    let rd = obj.get(key);
    console.log(`Full Data Requested ${socket.id}`);
    socket.emit('fullData', rd.data);
  });
  socket.on('attrChange', (pk) => {
    console.log('attrChange');
    let rd = obj.get(key);
    // if (rd.locks.hasOwnProperty(pk.src) && rd.locks[pk.src] == socket.id) {
    let data = findSrc(rd.data, pk.src);
    if (data == undefined) {
      rd.data.push({
        src: pk.src,
        time: [pk.time],
        data: [pk.data],
        max: 1,
        index: 0,
        current: pk.data
      });
      console.log(`Attr Added ${socket.id} - ${pk.src}`);
      console.log(`  ${JSON.stringify(pk.data)}`);
    } else {
      if (data.time.indexOf(pk.time) == -1) {
        data.max += 1;
        data.time.push(pk.time);
        data.time.sort();
      }
      let index = data.time.indexOf(pk.time);
      data.data.splice(index, 0, pk.data);
      console.log(`Attr Changed ${socket.id} - ${pk.src}`);
      console.log(`  ${JSON.stringify(pk.data)}`);
    }
    socket.broadcast.emit('attrChange', pk);
    // }
  });
  socket.on('print', () => {
    let rd = obj.get(key);
    console.log('locks')
    console.log(`  ${JSON.stringify(rd.locks)}`);
    console.log('data');
    console.log(JSON.stringify(rd.data));
  })
}
function findSrc(data, src) {
  for (let i = 0; i < data.length; i++)
    if (data[i].src == src)
      return data[i];
}